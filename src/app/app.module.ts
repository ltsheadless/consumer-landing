import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AllMaterialModule } from './all-material/all-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { SignInComponent } from './authentication/sign-in/sign-in.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthService } from './authservice/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { MyHttpInterceptor } from './http.interceptor';
import { ToastrModule } from 'ngx-toastr';
import { NotificationService } from './notifications/notification.service';
import { ConfirmpageComponent } from './consumercase/confirmpage/confirmpage.component';

@NgModule({
  declarations: [AppComponent, PagenotfoundComponent, SignInComponent, ConfirmpageComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AllMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard, NotificationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsumerlandingRoutingModule } from './consumerlanding-routing.module';
import { ConsumerlandingComponent } from './consumerlanding.component';
import { HomeComponent } from './home/home.component';
import { LoanComponent } from './loan/loan.component';
import { AllMaterialModule } from '../all-material/all-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { BoostrapmoduleModule } from '../boostrapmodule/boostrapmodule.module';
import { MyHttpInterceptor } from '../http.interceptor';
import { CaseService } from '../consumercase/services/case.service';

@NgModule({
  declarations: [ConsumerlandingComponent, HomeComponent, LoanComponent],
  imports: [
    CommonModule,
    ConsumerlandingRoutingModule,
    AllMaterialModule,
    BoostrapmoduleModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
  ],
  providers:[CaseService,{
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true,
  },]
})
export class ConsumerlandingModule {}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoanComponent } from './loan/loan.component';
import { ConsumerlandingComponent } from './consumerlanding.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: ConsumerlandingComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'loan', component: LoanComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerlandingRoutingModule {}

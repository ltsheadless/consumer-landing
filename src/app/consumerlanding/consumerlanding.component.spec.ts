import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumerlandingComponent } from './consumerlanding.component';

describe('ConsumerlandingComponent', () => {
  let component: ConsumerlandingComponent;
  let fixture: ComponentFixture<ConsumerlandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumerlandingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumerlandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

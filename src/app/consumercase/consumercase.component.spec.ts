import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumercaseComponent } from './consumercase.component';

describe('ConsumercaseComponent', () => {
  let component: ConsumercaseComponent;
  let fixture: ComponentFixture<ConsumercaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumercaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumercaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

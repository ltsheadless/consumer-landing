import { Options } from '@angular-slider/ngx-slider/options';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Consumerlanding } from 'src/app/consumerlanding.model';
import { NotificationService } from 'src/app/notifications/notification.service';
import { CaseService } from '../services/case.service';

@Component({
  selector: 'app-casedetail',
  templateUrl: './casedetail.component.html',
  styleUrls: ['./casedetail.component.css'],
})
export class CasedetailComponent implements OnInit {
  consumerlanding: Consumerlanding = new Consumerlanding();
  step: any;
  disabled!: boolean;
  isLinear = true;
  StepNxt = false;
  isCheckedApplicant = false;
  coAppliciantEmployeeDetails = false;
  coAppliciantFinancialDetails = false;
  kycVerifyData = 'Gray';
  firstFormGroup!: FormGroup;
  coapplicantFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  productForm!: FormGroup;
  propertyFormGroup!: FormGroup;
  loanFormGroup!: FormGroup;
  coapplicantSubmitted!: boolean;
  isCheckedApplicantData!: boolean;
  browwerdetailSubmitted!: boolean;
  employeedetailSubmitted!: boolean;

  quantityArrayAmount = [] as any;
  assetTotalSumAmount = 0;
  monthlyArrayAmount = [] as any;
  monthlyTotalSumAmount = 0;
  recurringArrayAmount = [] as any;
  recurringTotalSumAmount = 0;
  libertyArrayAmount = [] as any;
  libertyTotalSumAmount = 0;
  totalIncomeCumser = 0;
  totalIncomeCumserExp = 0;
  totalExpensiveCoApplicant = 0;
  saveBtn!: boolean;
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'before';
  coapplicant_assetTotalSumAmount = 0;
  coapplicant_monthlyTotalSumAmount = 0;
  coapplicant_recurringTotalSumAmount = 0;
  coapplicant_libertyTotalSumAmount = 0;
  totalIncomecoApplicant = 0;
  finnacialdetailSubmitted!: boolean;
  propertydetailSubmitted!: boolean;
  loandetailsSubmitted!: boolean;
  uploadFormGroup!: FormGroup;
  uploadDocumentSubmitted!: boolean;
  caseId: any;
  viewData: any;
  viewTemp!: boolean;
  viewLoanData: any;
  assetsDatas: any;
  monthllyDatas: any;
  recurrDatas: any;
  liabilDatas: any;
  viewPropertyData: any;
  nxtBtnShow = false;
  demoData: any;
  isDisable: boolean = true;
  selectDisable = false;
  loanSubmitted!: boolean;
  greenRedFlag!: boolean;
  bannerCaseName: any;
  loanProductSelectBox: any;
  userData: any;
  nxtBtnShowafterPM = false;

  constructor(
    private _formBuilder: FormBuilder,
    public datepipe: DatePipe,
    private caseService: CaseService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService,
    private spinner: NgxSpinnerService
  ) {}

  @ViewChild('stepper')
  stepper!: MatStepper;

  // ============== slider =====================

  value: number = 0;
  options: Options = {
    showTicksValues: true,
    disabled: false,
    stepsArray: [
      { value: 0 },
      { value: 5 },
      { value: 10 },
      { value: 15 },
      { value: 20 },
      { value: 25 },
      { value: 30 },
    ],
  };

  options1: Options = {
    showTicksValues: true,
    disabled: false,
    stepsArray: [
      { value: 0 },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
      { value: 6 },
      { value: 7 },
      { value: 8 },
      { value: 9 },
      { value: 10 },
      { value: 11 },
      { value: 12 },
    ],
  };

  options2: Options = {
    showTicksValues: true,
    disabled: false,
    stepsArray: [
      { value: 0 },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
    ],
  };

  // ============== slider =====================

  loanProspoes = [
    { value: 'BuildHouse', viewValue: 'Build a New House' },
    { value: 'BuyNewHouse', viewValue: 'Buy a New House' },
    { value: 'BuyanAppartment', viewValue: 'Buy an Apartment' },
    { value: 'RenovateHouse', viewValue: 'Renovate a House' },
    { value: 'RenovateanAppartment', viewValue: 'Renovate an Apartment' },
  ];

  loanProducts = [
    { value: 'MortgageCommitment', viewValue: 'Mortgage Commitment' },
    { value: 'Mortgage20yrsVilla', viewValue: 'Mortgage 20yrs Villa' },
    {
      value: 'Mortgage20yrsAppartment',
      viewValue: 'Mortgage 20yrs Apartment',
    },
    {
      value: 'MortgageLongTermRealEstate',
      viewValue: 'Mortgage Long Term Real Estate',
    },
    { value: 'MortgageRural', viewValue: 'Mortgage Rural' },
  ];

  countries = [
    { code: 'AF', code3: 'AFG', name: 'Afghanistan', number: '004' },
    { code: 'AL', code3: 'ALB', name: 'Albania', number: '008' },
    { code: 'DZ', code3: 'DZA', name: 'Algeria', number: '012' },
    { code: 'AS', code3: 'ASM', name: 'American Samoa', number: '016' },
    { code: 'AD', code3: 'AND', name: 'Andorra', number: '020' },
    { code: 'AO', code3: 'AGO', name: 'Angola', number: '024' },
    { code: 'AI', code3: 'AIA', name: 'Anguilla', number: '660' },
    { code: 'AQ', code3: 'ATA', name: 'Antarctica', number: '010' },
    { code: 'AG', code3: 'ATG', name: 'Antigua and Barbuda', number: '028' },
    { code: 'AR', code3: 'ARG', name: 'Argentina', number: '032' },
    { code: 'AM', code3: 'ARM', name: 'Armenia', number: '051' },
    { code: 'AW', code3: 'ABW', name: 'Aruba', number: '533' },
    { code: 'AU', code3: 'AUS', name: 'Australia', number: '036' },
    { code: 'AT', code3: 'AUT', name: 'Austria', number: '040' },
    { code: 'AZ', code3: 'AZE', name: 'Azerbaijan', number: '031' },
    { code: 'BS', code3: 'BHS', name: 'Bahamas (the)', number: '044' },
    { code: 'BH', code3: 'BHR', name: 'Bahrain', number: '048' },
    { code: 'BD', code3: 'BGD', name: 'Bangladesh', number: '050' },
    { code: 'BB', code3: 'BRB', name: 'Barbados', number: '052' },
    { code: 'BY', code3: 'BLR', name: 'Belarus', number: '112' },
    { code: 'BE', code3: 'BEL', name: 'Belgium', number: '056' },
    { code: 'BZ', code3: 'BLZ', name: 'Belize', number: '084' },
    { code: 'BJ', code3: 'BEN', name: 'Benin', number: '204' },
    { code: 'BM', code3: 'BMU', name: 'Bermuda', number: '060' },
    { code: 'BT', code3: 'BTN', name: 'Bhutan', number: '064' },
    {
      code: 'BO',
      code3: 'BOL',
      name: 'Bolivia (Plurinational State of)',
      number: '068',
    },
    {
      code: 'BQ',
      code3: 'BES',
      name: 'Bonaire, Sint Eustatius and Saba',
      number: '535',
    },
    { code: 'BA', code3: 'BIH', name: 'Bosnia and Herzegovina', number: '070' },
    { code: 'BW', code3: 'BWA', name: 'Botswana', number: '072' },
    { code: 'BV', code3: 'BVT', name: 'Bouvet Island', number: '074' },
    { code: 'BR', code3: 'BRA', name: 'Brazil', number: '076' },
    {
      code: 'IO',
      code3: 'IOT',
      name: 'British Indian Ocean Territory (the)',
      number: '086',
    },
    { code: 'BN', code3: 'BRN', name: 'Brunei Darussalam', number: '096' },
    { code: 'BG', code3: 'BGR', name: 'Bulgaria', number: '100' },
    { code: 'BF', code3: 'BFA', name: 'Burkina Faso', number: '854' },
    { code: 'BI', code3: 'BDI', name: 'Burundi', number: '108' },
    { code: 'CV', code3: 'CPV', name: 'Cabo Verde', number: '132' },
    { code: 'KH', code3: 'KHM', name: 'Cambodia', number: '116' },
    { code: 'CM', code3: 'CMR', name: 'Cameroon', number: '120' },
    { code: 'CA', code3: 'CAN', name: 'Canada', number: '124' },
    { code: 'KY', code3: 'CYM', name: 'Cayman Islands (the)', number: '136' },
    {
      code: 'CF',
      code3: 'CAF',
      name: 'Central African Republic (the)',
      number: '140',
    },
    { code: 'TD', code3: 'TCD', name: 'Chad', number: '148' },
    { code: 'CL', code3: 'CHL', name: 'Chile', number: '152' },
    { code: 'CN', code3: 'CHN', name: 'China', number: '156' },
    { code: 'CX', code3: 'CXR', name: 'Christmas Island', number: '162' },
    {
      code: 'CC',
      code3: 'CCK',
      name: 'Cocos (Keeling) Islands (the)',
      number: '166',
    },
    { code: 'CO', code3: 'COL', name: 'Colombia', number: '170' },
    { code: 'KM', code3: 'COM', name: 'Comoros (the)', number: '174' },
    {
      code: 'CD',
      code3: 'COD',
      name: 'Congo (the Democratic Republic of the)',
      number: '180',
    },
    { code: 'CG', code3: 'COG', name: 'Congo (the)', number: '178' },
    { code: 'CK', code3: 'COK', name: 'Cook Islands (the)', number: '184' },
    { code: 'CR', code3: 'CRI', name: 'Costa Rica', number: '188' },
    { code: 'HR', code3: 'HRV', name: 'Croatia', number: '191' },
    { code: 'CU', code3: 'CUB', name: 'Cuba', number: '192' },
    { code: 'CW', code3: 'CUW', name: 'Curaçao', number: '531' },
    { code: 'CY', code3: 'CYP', name: 'Cyprus', number: '196' },
    { code: 'CZ', code3: 'CZE', name: 'Czechia', number: '203' },
    { code: 'CI', code3: 'CIV', name: "Côte d'Ivoire", number: '384' },
    { code: 'DK', code3: 'DNK', name: 'Denmark', number: '208' },
    { code: 'DJ', code3: 'DJI', name: 'Djibouti', number: '262' },
    { code: 'DM', code3: 'DMA', name: 'Dominica', number: '212' },
    {
      code: 'DO',
      code3: 'DOM',
      name: 'Dominican Republic (the)',
      number: '214',
    },
    { code: 'EC', code3: 'ECU', name: 'Ecuador', number: '218' },
    { code: 'EG', code3: 'EGY', name: 'Egypt', number: '818' },
    { code: 'SV', code3: 'SLV', name: 'El Salvador', number: '222' },
    { code: 'GQ', code3: 'GNQ', name: 'Equatorial Guinea', number: '226' },
    { code: 'ER', code3: 'ERI', name: 'Eritrea', number: '232' },
    { code: 'EE', code3: 'EST', name: 'Estonia', number: '233' },
    { code: 'SZ', code3: 'SWZ', name: 'Eswatini', number: '748' },
    { code: 'ET', code3: 'ETH', name: 'Ethiopia', number: '231' },
    {
      code: 'FK',
      code3: 'FLK',
      name: 'Falkland Islands (the) [Malvinas]',
      number: '238',
    },
    { code: 'FO', code3: 'FRO', name: 'Faroe Islands (the)', number: '234' },
    { code: 'FJ', code3: 'FJI', name: 'Fiji', number: '242' },
    { code: 'FI', code3: 'FIN', name: 'Finland', number: '246' },
    { code: 'FR', code3: 'FRA', name: 'France', number: '250' },
    { code: 'GF', code3: 'GUF', name: 'French Guiana', number: '254' },
    { code: 'PF', code3: 'PYF', name: 'French Polynesia', number: '258' },
    {
      code: 'TF',
      code3: 'ATF',
      name: 'French Southern Territories (the)',
      number: '260',
    },
    { code: 'GA', code3: 'GAB', name: 'Gabon', number: '266' },
    { code: 'GM', code3: 'GMB', name: 'Gambia (the)', number: '270' },
    { code: 'GE', code3: 'GEO', name: 'Georgia', number: '268' },
    { code: 'DE', code3: 'DEU', name: 'Germany', number: '276' },
    { code: 'GH', code3: 'GHA', name: 'Ghana', number: '288' },
    { code: 'GI', code3: 'GIB', name: 'Gibraltar', number: '292' },
    { code: 'GR', code3: 'GRC', name: 'Greece', number: '300' },
    { code: 'GL', code3: 'GRL', name: 'Greenland', number: '304' },
    { code: 'GD', code3: 'GRD', name: 'Grenada', number: '308' },
    { code: 'GP', code3: 'GLP', name: 'Guadeloupe', number: '312' },
    { code: 'GU', code3: 'GUM', name: 'Guam', number: '316' },
    { code: 'GT', code3: 'GTM', name: 'Guatemala', number: '320' },
    { code: 'GG', code3: 'GGY', name: 'Guernsey', number: '831' },
    { code: 'GN', code3: 'GIN', name: 'Guinea', number: '324' },
    { code: 'GW', code3: 'GNB', name: 'Guinea-Bissau', number: '624' },
    { code: 'GY', code3: 'GUY', name: 'Guyana', number: '328' },
    { code: 'HT', code3: 'HTI', name: 'Haiti', number: '332' },
    {
      code: 'HM',
      code3: 'HMD',
      name: 'Heard Island and McDonald Islands',
      number: '334',
    },
    { code: 'VA', code3: 'VAT', name: 'Holy See (the)', number: '336' },
    { code: 'HN', code3: 'HND', name: 'Honduras', number: '340' },
    { code: 'HK', code3: 'HKG', name: 'Hong Kong', number: '344' },
    { code: 'HU', code3: 'HUN', name: 'Hungary', number: '348' },
    { code: 'IS', code3: 'ISL', name: 'Iceland', number: '352' },
    { code: 'IN', code3: 'IND', name: 'India', number: '356' },
    { code: 'ID', code3: 'IDN', name: 'Indonesia', number: '360' },
    {
      code: 'IR',
      code3: 'IRN',
      name: 'Iran (Islamic Republic of)',
      number: '364',
    },
    { code: 'IQ', code3: 'IRQ', name: 'Iraq', number: '368' },
    { code: 'IE', code3: 'IRL', name: 'Ireland', number: '372' },
    { code: 'IM', code3: 'IMN', name: 'Isle of Man', number: '833' },
    { code: 'IL', code3: 'ISR', name: 'Israel', number: '376' },
    { code: 'IT', code3: 'ITA', name: 'Italy', number: '380' },
    { code: 'JM', code3: 'JAM', name: 'Jamaica', number: '388' },
    { code: 'JP', code3: 'JPN', name: 'Japan', number: '392' },
    { code: 'JE', code3: 'JEY', name: 'Jersey', number: '832' },
    { code: 'JO', code3: 'JOR', name: 'Jordan', number: '400' },
    { code: 'KZ', code3: 'KAZ', name: 'Kazakhstan', number: '398' },
    { code: 'KE', code3: 'KEN', name: 'Kenya', number: '404' },
    { code: 'KI', code3: 'KIR', name: 'Kiribati', number: '296' },
    {
      code: 'KP',
      code3: 'PRK',
      name: "Korea (the Democratic People's Republic of)",
      number: '408',
    },
    {
      code: 'KR',
      code3: 'KOR',
      name: 'Korea (the Republic of)',
      number: '410',
    },
    { code: 'KW', code3: 'KWT', name: 'Kuwait', number: '414' },
    { code: 'KG', code3: 'KGZ', name: 'Kyrgyzstan', number: '417' },
    {
      code: 'LA',
      code3: 'LAO',
      name: "Lao People's Democratic Republic (the)",
      number: '418',
    },
    { code: 'LV', code3: 'LVA', name: 'Latvia', number: '428' },
    { code: 'LB', code3: 'LBN', name: 'Lebanon', number: '422' },
    { code: 'LS', code3: 'LSO', name: 'Lesotho', number: '426' },
    { code: 'LR', code3: 'LBR', name: 'Liberia', number: '430' },
    { code: 'LY', code3: 'LBY', name: 'Libya', number: '434' },
    { code: 'LI', code3: 'LIE', name: 'Liechtenstein', number: '438' },
    { code: 'LT', code3: 'LTU', name: 'Lithuania', number: '440' },
    { code: 'LU', code3: 'LUX', name: 'Luxembourg', number: '442' },
    { code: 'MO', code3: 'MAC', name: 'Macao', number: '446' },
    { code: 'MG', code3: 'MDG', name: 'Madagascar', number: '450' },
    { code: 'MW', code3: 'MWI', name: 'Malawi', number: '454' },
    { code: 'MY', code3: 'MYS', name: 'Malaysia', number: '458' },
    { code: 'MV', code3: 'MDV', name: 'Maldives', number: '462' },
    { code: 'ML', code3: 'MLI', name: 'Mali', number: '466' },
    { code: 'MT', code3: 'MLT', name: 'Malta', number: '470' },
    { code: 'MH', code3: 'MHL', name: 'Marshall Islands (the)', number: '584' },
    { code: 'MQ', code3: 'MTQ', name: 'Martinique', number: '474' },
    { code: 'MR', code3: 'MRT', name: 'Mauritania', number: '478' },
    { code: 'MU', code3: 'MUS', name: 'Mauritius', number: '480' },
    { code: 'YT', code3: 'MYT', name: 'Mayotte', number: '175' },
    { code: 'MX', code3: 'MEX', name: 'Mexico', number: '484' },
    {
      code: 'FM',
      code3: 'FSM',
      name: 'Micronesia (Federated States of)',
      number: '583',
    },
    {
      code: 'MD',
      code3: 'MDA',
      name: 'Moldova (the Republic of)',
      number: '498',
    },
    { code: 'MC', code3: 'MCO', name: 'Monaco', number: '492' },
    { code: 'MN', code3: 'MNG', name: 'Mongolia', number: '496' },
    { code: 'ME', code3: 'MNE', name: 'Montenegro', number: '499' },
    { code: 'MS', code3: 'MSR', name: 'Montserrat', number: '500' },
    { code: 'MA', code3: 'MAR', name: 'Morocco', number: '504' },
    { code: 'MZ', code3: 'MOZ', name: 'Mozambique', number: '508' },
    { code: 'MM', code3: 'MMR', name: 'Myanmar', number: '104' },
    { code: 'NA', code3: 'NAM', name: 'Namibia', number: '516' },
    { code: 'NR', code3: 'NRU', name: 'Nauru', number: '520' },
    { code: 'NP', code3: 'NPL', name: 'Nepal', number: '524' },
    { code: 'NL', code3: 'NLD', name: 'Netherlands (the)', number: '528' },
    { code: 'NC', code3: 'NCL', name: 'New Caledonia', number: '540' },
    { code: 'NZ', code3: 'NZL', name: 'New Zealand', number: '554' },
    { code: 'NI', code3: 'NIC', name: 'Nicaragua', number: '558' },
    { code: 'NE', code3: 'NER', name: 'Niger (the)', number: '562' },
    { code: 'NG', code3: 'NGA', name: 'Nigeria', number: '566' },
    { code: 'NU', code3: 'NIU', name: 'Niue', number: '570' },
    { code: 'NF', code3: 'NFK', name: 'Norfolk Island', number: '574' },
    {
      code: 'MP',
      code3: 'MNP',
      name: 'Northern Mariana Islands (the)',
      number: '580',
    },
    { code: 'NO', code3: 'NOR', name: 'Norway', number: '578' },
    { code: 'OM', code3: 'OMN', name: 'Oman', number: '512' },
    { code: 'PK', code3: 'PAK', name: 'Pakistan', number: '586' },
    { code: 'PW', code3: 'PLW', name: 'Palau', number: '585' },
    { code: 'PS', code3: 'PSE', name: 'Palestine, State of', number: '275' },
    { code: 'PA', code3: 'PAN', name: 'Panama', number: '591' },
    { code: 'PG', code3: 'PNG', name: 'Papua New Guinea', number: '598' },
    { code: 'PY', code3: 'PRY', name: 'Paraguay', number: '600' },
    { code: 'PE', code3: 'PER', name: 'Peru', number: '604' },
    { code: 'PH', code3: 'PHL', name: 'Philippines (the)', number: '608' },
    { code: 'PN', code3: 'PCN', name: 'Pitcairn', number: '612' },
    { code: 'PL', code3: 'POL', name: 'Poland', number: '616' },
    { code: 'PT', code3: 'PRT', name: 'Portugal', number: '620' },
    { code: 'PR', code3: 'PRI', name: 'Puerto Rico', number: '630' },
    { code: 'QA', code3: 'QAT', name: 'Qatar', number: '634' },
    {
      code: 'MK',
      code3: 'MKD',
      name: 'Republic of North Macedonia',
      number: '807',
    },
    { code: 'RO', code3: 'ROU', name: 'Romania', number: '642' },
    {
      code: 'RU',
      code3: 'RUS',
      name: 'Russian Federation (the)',
      number: '643',
    },
    { code: 'RW', code3: 'RWA', name: 'Rwanda', number: '646' },
    { code: 'RE', code3: 'REU', name: 'Réunion', number: '638' },
    { code: 'BL', code3: 'BLM', name: 'Saint Barthélemy', number: '652' },
    {
      code: 'SH',
      code3: 'SHN',
      name: 'Saint Helena, Ascension and Tristan da Cunha',
      number: '654',
    },
    { code: 'KN', code3: 'KNA', name: 'Saint Kitts and Nevis', number: '659' },
    { code: 'LC', code3: 'LCA', name: 'Saint Lucia', number: '662' },
    {
      code: 'MF',
      code3: 'MAF',
      name: 'Saint Martin (French part)',
      number: '663',
    },
    {
      code: 'PM',
      code3: 'SPM',
      name: 'Saint Pierre and Miquelon',
      number: '666',
    },
    {
      code: 'VC',
      code3: 'VCT',
      name: 'Saint Vincent and the Grenadines',
      number: '670',
    },
    { code: 'WS', code3: 'WSM', name: 'Samoa', number: '882' },
    { code: 'SM', code3: 'SMR', name: 'San Marino', number: '674' },
    { code: 'ST', code3: 'STP', name: 'Sao Tome and Principe', number: '678' },
    { code: 'SA', code3: 'SAU', name: 'Saudi Arabia', number: '682' },
    { code: 'SN', code3: 'SEN', name: 'Senegal', number: '686' },
    { code: 'RS', code3: 'SRB', name: 'Serbia', number: '688' },
    { code: 'SC', code3: 'SYC', name: 'Seychelles', number: '690' },
    { code: 'SL', code3: 'SLE', name: 'Sierra Leone', number: '694' },
    { code: 'SG', code3: 'SGP', name: 'Singapore', number: '702' },
    {
      code: 'SX',
      code3: 'SXM',
      name: 'Sint Maarten (Dutch part)',
      number: '534',
    },
    { code: 'SK', code3: 'SVK', name: 'Slovakia', number: '703' },
    { code: 'SI', code3: 'SVN', name: 'Slovenia', number: '705' },
    { code: 'SB', code3: 'SLB', name: 'Solomon Islands', number: '090' },
    { code: 'SO', code3: 'SOM', name: 'Somalia', number: '706' },
    { code: 'ZA', code3: 'ZAF', name: 'South Africa', number: '710' },
    {
      code: 'GS',
      code3: 'SGS',
      name: 'South Georgia and the South Sandwich Islands',
      number: '239',
    },
    { code: 'SS', code3: 'SSD', name: 'South Sudan', number: '728' },
    { code: 'ES', code3: 'ESP', name: 'Spain', number: '724' },
    { code: 'LK', code3: 'LKA', name: 'Sri Lanka', number: '144' },
    { code: 'SD', code3: 'SDN', name: 'Sudan (the)', number: '729' },
    { code: 'SR', code3: 'SUR', name: 'Suriname', number: '740' },
    { code: 'SJ', code3: 'SJM', name: 'Svalbard and Jan Mayen', number: '744' },
    { code: 'SE', code3: 'SWE', name: 'Sweden', number: '752' },
    { code: 'CH', code3: 'CHE', name: 'Switzerland', number: '756' },
    { code: 'SY', code3: 'SYR', name: 'Syrian Arab Republic', number: '760' },
    { code: 'TW', code3: 'TWN', name: 'Taiwan', number: '158' },
    { code: 'TJ', code3: 'TJK', name: 'Tajikistan', number: '762' },
    {
      code: 'TZ',
      code3: 'TZA',
      name: 'Tanzania, United Republic of',
      number: '834',
    },
    { code: 'TH', code3: 'THA', name: 'Thailand', number: '764' },
    { code: 'TL', code3: 'TLS', name: 'Timor-Leste', number: '626' },
    { code: 'TG', code3: 'TGO', name: 'Togo', number: '768' },
    { code: 'TK', code3: 'TKL', name: 'Tokelau', number: '772' },
    { code: 'TO', code3: 'TON', name: 'Tonga', number: '776' },
    { code: 'TT', code3: 'TTO', name: 'Trinidad and Tobago', number: '780' },
    { code: 'TN', code3: 'TUN', name: 'Tunisia', number: '788' },
    { code: 'TR', code3: 'TUR', name: 'Turkey', number: '792' },
    { code: 'TM', code3: 'TKM', name: 'Turkmenistan', number: '795' },
    {
      code: 'TC',
      code3: 'TCA',
      name: 'Turks and Caicos Islands (the)',
      number: '796',
    },
    { code: 'TV', code3: 'TUV', name: 'Tuvalu', number: '798' },
    { code: 'UG', code3: 'UGA', name: 'Uganda', number: '800' },
    { code: 'UA', code3: 'UKR', name: 'Ukraine', number: '804' },
    {
      code: 'AE',
      code3: 'ARE',
      name: 'United Arab Emirates (the)',
      number: '784',
    },
    {
      code: 'GB',
      code3: 'GBR',
      name: 'United Kingdom of Great Britain and Northern Ireland (the)',
      number: '826',
    },
    {
      code: 'UM',
      code3: 'UMI',
      name: 'United States Minor Outlying Islands (the)',
      number: '581',
    },
    {
      code: 'US',
      code3: 'USA',
      name: 'United States of America (the)',
      number: '840',
    },
    { code: 'UY', code3: 'URY', name: 'Uruguay', number: '858' },
    { code: 'UZ', code3: 'UZB', name: 'Uzbekistan', number: '860' },
    { code: 'VU', code3: 'VUT', name: 'Vanuatu', number: '548' },
    {
      code: 'VE',
      code3: 'VEN',
      name: 'Venezuela (Bolivarian Republic of)',
      number: '862',
    },
    { code: 'VN', code3: 'VNM', name: 'Viet Nam', number: '704' },
    {
      code: 'VG',
      code3: 'VGB',
      name: 'Virgin Islands (British)',
      number: '092',
    },
    { code: 'VI', code3: 'VIR', name: 'Virgin Islands (U.S.)', number: '850' },
    { code: 'WF', code3: 'WLF', name: 'Wallis and Futuna', number: '876' },
    { code: 'EH', code3: 'ESH', name: 'Western Sahara', number: '732' },
    { code: 'YE', code3: 'YEM', name: 'Yemen', number: '887' },
    { code: 'ZM', code3: 'ZMB', name: 'Zambia', number: '894' },
    { code: 'ZW', code3: 'ZWE', name: 'Zimbabwe', number: '716' },
    { code: 'AX', code3: 'ALA', name: 'Åland Islands', number: '248' },
  ];

  cities = [
    { value: 'Blr', viewValue: 'Blr' },
    { value: 'London', viewValue: 'London' },
    { value: 'Melbourne', viewValue: 'Melbourne' },
    { value: 'Stockholm', viewValue: 'Stockholm' },
  ];

  employees = [
    { value: 'hourlybasis', viewValue: 'Hourly Basis' },
    { value: 'onprobation', viewValue: 'On Probation' },
    { value: 'pensioner', viewValue: 'Pensioner' },
    { value: 'PermanentEmployee', viewValue: 'Permanent Employee' },
    { value: 'student', viewValue: 'Student' },
    { value: 'selfemployed', viewValue: 'Self Employed' },
  ];

  finacials = [
    { value: 'PR', viewValue: 'Property' },
    { value: 'SB', viewValue: 'Stocks & Bonds' },
    { value: 'OT', viewValue: 'Others' },
  ];

  sourcefinacials = [
    { value: 'BI', viewValue: 'Busness Income' },
    {
      value: 'CG',
      viewValue: 'Capital Gains from Stocks & Bonds',
    },
    { value: 'II', viewValue: 'Interest Income' },
    { value: 'RI', viewValue: 'Rental Income' },
    { value: 'O', viewValue: 'Others' },
  ];

  recurringfinacials = [
    {
      value: 'Average Household Expense',
      viewValue: 'Average HouseHold Expense',
    },
    { value: 'Medical Expense', viewValue: 'Medical Expense' },
    { value: 'Rental Expense', viewValue: 'Rental Expense' },
    { value: 'Other Expense', viewValue: 'Other Expense' },
  ];
  liabilitfinacials = [
    { value: 'Car Loan', viewValue: 'Car Loan' },
    { value: 'House Loan', viewValue: 'House Loan' },
    { value: 'Others', viewValue: 'Others' },
  ];

  propertyTypes = [
    { value: 'Condo', viewValue: 'Condo' },
    { value: 'SFR', viewValue: 'SFR' },
    { value: 'Villa', viewValue: 'Villa' },
  ];

  properties = [
    { value: 'owned', viewValue: 'Owned' },
    { value: 'rental', viewValue: 'Rental' },
  ];

  documentTypes = [
    { value: 'employmentcertificate', viewValue: 'Employment Certificate' },
    { value: 'identificationproof', viewValue: 'Identification Proof' },
    { value: 'legaldocuments', viewValue: 'Legal Documents' },
    { value: 'salarystatement', viewValue: 'Salary Statement' },
  ];

  selectItem(e: any) {
    if (e === true) {
      this.isCheckedApplicant = true;
      //this.saveBtn = true;
    } else {
      this.isCheckedApplicant = false;
      this.isCheckedApplicantData = false;
      this.coAppliciantEmployeeDetails = false;
      this.coAppliciantFinancialDetails = false;
      // this.saveBtn = false;
    }
  }

  currencySymbols = [
    {
      countryCode: 'AD',
      countryName: 'Andorra',
      currencyCode: 'EUR',
      population: '84000',
      capital: 'Andorra la Vella',
      continentName: 'Europe',
    },
    {
      countryCode: 'AE',
      countryName: 'United Arab Emirates',
      currencyCode: 'AED',
      population: '4975593',
      capital: 'Abu Dhabi',
      continentName: 'Asia',
    },
    {
      countryCode: 'AF',
      countryName: 'Afghanistan',
      currencyCode: 'AFN',
      population: '29121286',
      capital: 'Kabul',
      continentName: 'Asia',
    },
    {
      countryCode: 'AG',
      countryName: 'Antigua and Barbuda',
      currencyCode: 'XCD',
      population: '86754',
      capital: "St. John's",
      continentName: 'North America',
    },

    {
      countryCode: 'AL',
      countryName: 'Albania',
      currencyCode: 'ALL',
      population: '2986952',
      capital: 'Tirana',
      continentName: 'Europe',
    },
    {
      countryCode: 'AM',
      countryName: 'Armenia',
      currencyCode: 'AMD',
      population: '2968000',
      capital: 'Yerevan',
      continentName: 'Asia',
    },
    {
      countryCode: 'AO',
      countryName: 'Angola',
      currencyCode: 'AOA',
      population: '13068161',
      capital: 'Luanda',
      continentName: 'Africa',
    },

    {
      countryCode: 'AR',
      countryName: 'Argentina',
      currencyCode: 'ARS',
      population: '41343201',
      capital: 'Buenos Aires',
      continentName: 'South America',
    },
    {
      countryCode: 'AS',
      countryName: 'American Samoa',
      currencyCode: 'USD',
      population: '57881',
      capital: 'Pago Pago',
      continentName: 'Oceania',
    },

    {
      countryCode: 'AU',
      countryName: 'Australia',
      currencyCode: 'AUD',
      population: '21515754',
      capital: 'Canberra',
      continentName: 'Oceania',
    },
    {
      countryCode: 'AW',
      countryName: 'Aruba',
      currencyCode: 'AWG',
      population: '71566',
      capital: 'Oranjestad',
      continentName: 'North America',
    },

    {
      countryCode: 'AZ',
      countryName: 'Azerbaijan',
      currencyCode: 'AZN',
      population: '8303512',
      capital: 'Baku',
      continentName: 'Asia',
    },
    {
      countryCode: 'BA',
      countryName: 'Bosnia and Herzegovina',
      currencyCode: 'BAM',
      population: '4590000',
      capital: 'Sarajevo',
      continentName: 'Europe',
    },
    {
      countryCode: 'BB',
      countryName: 'Barbados',
      currencyCode: 'BBD',
      population: '285653',
      capital: 'Bridgetown',
      continentName: 'North America',
    },
    {
      countryCode: 'BD',
      countryName: 'Bangladesh',
      currencyCode: 'BDT',
      population: '156118464',
      capital: 'Dhaka',
      continentName: 'Asia',
    },

    {
      countryCode: 'BF',
      countryName: 'Burkina Faso',
      currencyCode: 'XOF',
      population: '16241811',
      capital: 'Ouagadougou',
      continentName: 'Africa',
    },
    {
      countryCode: 'BG',
      countryName: 'Bulgaria',
      currencyCode: 'BGN',
      population: '7148785',
      capital: 'Sofia',
      continentName: 'Europe',
    },
    {
      countryCode: 'BH',
      countryName: 'Bahrain',
      currencyCode: 'BHD',
      population: '738004',
      capital: 'Manama',
      continentName: 'Asia',
    },
    {
      countryCode: 'BI',
      countryName: 'Burundi',
      currencyCode: 'BIF',
      population: '9863117',
      capital: 'Bujumbura',
      continentName: 'Africa',
    },

    {
      countryCode: 'BM',
      countryName: 'Bermuda',
      currencyCode: 'BMD',
      population: '65365',
      capital: 'Hamilton',
      continentName: 'North America',
    },
    {
      countryCode: 'BN',
      countryName: 'Brunei',
      currencyCode: 'BND',
      population: '395027',
      capital: 'Bandar Seri Begawan',
      continentName: 'Asia',
    },
    {
      countryCode: 'BO',
      countryName: 'Bolivia',
      currencyCode: 'BOB',
      population: '9947418',
      capital: 'Sucre',
      continentName: 'South America',
    },

    {
      countryCode: 'BR',
      countryName: 'Brazil',
      currencyCode: 'BRL',
      population: '201103330',
      capital: 'Brasília',
      continentName: 'South America',
    },
    {
      countryCode: 'BS',
      countryName: 'Bahamas',
      currencyCode: 'BSD',
      population: '301790',
      capital: 'Nassau',
      continentName: 'North America',
    },
    {
      countryCode: 'BT',
      countryName: 'Bhutan',
      currencyCode: 'BTN',
      population: '699847',
      capital: 'Thimphu',
      continentName: 'Asia',
    },
    {
      countryCode: 'BV',
      countryName: 'Bouvet Island',
      currencyCode: 'NOK',
      population: '0',
      capital: '',
      continentName: 'Antarctica',
    },
    {
      countryCode: 'BW',
      countryName: 'Botswana',
      currencyCode: 'BWP',
      population: '2029307',
      capital: 'Gaborone',
      continentName: 'Africa',
    },
    {
      countryCode: 'BY',
      countryName: 'Belarus',
      currencyCode: 'BYR',
      population: '9685000',
      capital: 'Minsk',
      continentName: 'Europe',
    },
    {
      countryCode: 'BZ',
      countryName: 'Belize',
      currencyCode: 'BZD',
      population: '314522',
      capital: 'Belmopan',
      continentName: 'North America',
    },
    {
      countryCode: 'CA',
      countryName: 'Canada',
      currencyCode: 'CAD',
      population: '33679000',
      capital: 'Ottawa',
      continentName: 'North America',
    },

    {
      countryCode: 'CD',
      countryName: 'Democratic Republic of the Congo',
      currencyCode: 'CDF',
      population: '70916439',
      capital: 'Kinshasa',
      continentName: 'Africa',
    },
    {
      countryCode: 'CF',
      countryName: 'Central African Republic',
      currencyCode: 'XAF',
      population: '4844927',
      capital: 'Bangui',
      continentName: 'Africa',
    },

    {
      countryCode: 'CH',
      countryName: 'Switzerland',
      currencyCode: 'CHF',
      population: '7581000',
      capital: 'Bern',
      continentName: 'Europe',
    },

    {
      countryCode: 'CK',
      countryName: 'Cook Islands',
      currencyCode: 'NZD',
      population: '21388',
      capital: 'Avarua',
      continentName: 'Oceania',
    },
    {
      countryCode: 'CL',
      countryName: 'Chile',
      currencyCode: 'CLP',
      population: '16746491',
      capital: 'Santiago',
      continentName: 'South America',
    },

    {
      countryCode: 'CN',
      countryName: 'China',
      currencyCode: 'CNY',
      population: '1330044000',
      capital: 'Beijing',
      continentName: 'Asia',
    },
    {
      countryCode: 'CO',
      countryName: 'Colombia',
      currencyCode: 'COP',
      population: '47790000',
      capital: 'Bogotá',
      continentName: 'South America',
    },
    {
      countryCode: 'CR',
      countryName: 'Costa Rica',
      currencyCode: 'CRC',
      population: '4516220',
      capital: 'San José',
      continentName: 'North America',
    },
    {
      countryCode: 'CU',
      countryName: 'Cuba',
      currencyCode: 'CUP',
      population: '11423000',
      capital: 'Havana',
      continentName: 'North America',
    },
    {
      countryCode: 'CV',
      countryName: 'Cape Verde',
      currencyCode: 'CVE',
      population: '508659',
      capital: 'Praia',
      continentName: 'Africa',
    },
    {
      countryCode: 'CW',
      countryName: 'Curacao',
      currencyCode: 'ANG',
      population: '141766',
      capital: 'Willemstad',
      continentName: 'North America',
    },

    {
      countryCode: 'CZ',
      countryName: 'Czechia',
      currencyCode: 'CZK',
      population: '10476000',
      capital: 'Prague',
      continentName: 'Europe',
    },

    {
      countryCode: 'DJ',
      countryName: 'Djibouti',
      currencyCode: 'DJF',
      population: '740528',
      capital: 'Djibouti',
      continentName: 'Africa',
    },
    {
      countryCode: 'DK',
      countryName: 'Denmark',
      currencyCode: 'DKK',
      population: '5484000',
      capital: 'Copenhagen',
      continentName: 'Europe',
    },

    {
      countryCode: 'DO',
      countryName: 'Dominican Republic',
      currencyCode: 'DOP',
      population: '9823821',
      capital: 'Santo Domingo',
      continentName: 'North America',
    },
    {
      countryCode: 'DZ',
      countryName: 'Algeria',
      currencyCode: 'DZD',
      population: '34586184',
      capital: 'Algiers',
      continentName: 'Africa',
    },

    {
      countryCode: 'EG',
      countryName: 'Egypt',
      currencyCode: 'EGP',
      population: '80471869',
      capital: 'Cairo',
      continentName: 'Africa',
    },
    {
      countryCode: 'EH',
      countryName: 'Western Sahara',
      currencyCode: 'MAD',
      population: '273008',
      capital: 'Laâyoune / El Aaiún',
      continentName: 'Africa',
    },
    {
      countryCode: 'ER',
      countryName: 'Eritrea',
      currencyCode: 'ERN',
      population: '5792984',
      capital: 'Asmara',
      continentName: 'Africa',
    },

    {
      countryCode: 'ET',
      countryName: 'Ethiopia',
      currencyCode: 'ETB',
      population: '88013491',
      capital: 'Addis Ababa',
      continentName: 'Africa',
    },

    {
      countryCode: 'FJ',
      countryName: 'Fiji',
      currencyCode: 'FJD',
      population: '875983',
      capital: 'Suva',
      continentName: 'Oceania',
    },
    {
      countryCode: 'FK',
      countryName: 'Falkland Islands',
      currencyCode: 'FKP',
      population: '2638',
      capital: 'Stanley',
      continentName: 'South America',
    },

    {
      countryCode: 'GB',
      countryName: 'United Kingdom',
      currencyCode: 'GBP',
      population: '62348447',
      capital: 'London',
      continentName: 'Europe',
    },

    {
      countryCode: 'GE',
      countryName: 'Georgia',
      currencyCode: 'GEL',
      population: '4630000',
      capital: 'Tbilisi',
      continentName: 'Asia',
    },

    {
      countryCode: 'GH',
      countryName: 'Ghana',
      currencyCode: 'GHS',
      population: '24339838',
      capital: 'Accra',
      continentName: 'Africa',
    },
    {
      countryCode: 'GI',
      countryName: 'Gibraltar',
      currencyCode: 'GIP',
      population: '27884',
      capital: 'Gibraltar',
      continentName: 'Europe',
    },

    {
      countryCode: 'GM',
      countryName: 'Gambia',
      currencyCode: 'GMD',
      population: '1593256',
      capital: 'Bathurst',
      continentName: 'Africa',
    },
    {
      countryCode: 'GN',
      countryName: 'Guinea',
      currencyCode: 'GNF',
      population: '10324025',
      capital: 'Conakry',
      continentName: 'Africa',
    },

    {
      countryCode: 'GT',
      countryName: 'Guatemala',
      currencyCode: 'GTQ',
      population: '13550440',
      capital: 'Guatemala City',
      continentName: 'North America',
    },

    {
      countryCode: 'GY',
      countryName: 'Guyana',
      currencyCode: 'GYD',
      population: '748486',
      capital: 'Georgetown',
      continentName: 'South America',
    },
    {
      countryCode: 'HK',
      countryName: 'Hong Kong',
      currencyCode: 'HKD',
      population: '6898686',
      capital: 'Hong Kong',
      continentName: 'Asia',
    },

    {
      countryCode: 'HN',
      countryName: 'Honduras',
      currencyCode: 'HNL',
      population: '7989415',
      capital: 'Tegucigalpa',
      continentName: 'North America',
    },
    {
      countryCode: 'HR',
      countryName: 'Croatia',
      currencyCode: 'HRK',
      population: '4284889',
      capital: 'Zagreb',
      continentName: 'Europe',
    },
    {
      countryCode: 'HT',
      countryName: 'Haiti',
      currencyCode: 'HTG',
      population: '9648924',
      capital: 'Port-au-Prince',
      continentName: 'North America',
    },
    {
      countryCode: 'HU',
      countryName: 'Hungary',
      currencyCode: 'HUF',
      population: '9982000',
      capital: 'Budapest',
      continentName: 'Europe',
    },
    {
      countryCode: 'ID',
      countryName: 'Indonesia',
      currencyCode: 'IDR',
      population: '242968342',
      capital: 'Jakarta',
      continentName: 'Asia',
    },

    {
      countryCode: 'IL',
      countryName: 'Israel',
      currencyCode: 'ILS',
      population: '7353985',
      capital: '',
      continentName: 'Asia',
    },

    {
      countryCode: 'IN',
      countryName: 'India',
      currencyCode: 'INR',
      population: '1173108018',
      capital: 'New Delhi',
      continentName: 'Asia',
    },

    {
      countryCode: 'IQ',
      countryName: 'Iraq',
      currencyCode: 'IQD',
      population: '29671605',
      capital: 'Baghdad',
      continentName: 'Asia',
    },
    {
      countryCode: 'IR',
      countryName: 'Iran',
      currencyCode: 'IRR',
      population: '76923300',
      capital: 'Tehran',
      continentName: 'Asia',
    },
    {
      countryCode: 'IS',
      countryName: 'Iceland',
      currencyCode: 'ISK',
      population: '308910',
      capital: 'Reykjavik',
      continentName: 'Europe',
    },

    {
      countryCode: 'JM',
      countryName: 'Jamaica',
      currencyCode: 'JMD',
      population: '2847232',
      capital: 'Kingston',
      continentName: 'North America',
    },
    {
      countryCode: 'JO',
      countryName: 'Jordan',
      currencyCode: 'JOD',
      population: '6407085',
      capital: 'Amman',
      continentName: 'Asia',
    },
    {
      countryCode: 'JP',
      countryName: 'Japan',
      currencyCode: 'JPY',
      population: '127288000',
      capital: 'Tokyo',
      continentName: 'Asia',
    },
    {
      countryCode: 'KE',
      countryName: 'Kenya',
      currencyCode: 'KES',
      population: '40046566',
      capital: 'Nairobi',
      continentName: 'Africa',
    },
    {
      countryCode: 'KG',
      countryName: 'Kyrgyzstan',
      currencyCode: 'KGS',
      population: '5776500',
      capital: 'Bishkek',
      continentName: 'Asia',
    },
    {
      countryCode: 'KH',
      countryName: 'Cambodia',
      currencyCode: 'KHR',
      population: '14453680',
      capital: 'Phnom Penh',
      continentName: 'Asia',
    },

    {
      countryCode: 'KM',
      countryName: 'Comoros',
      currencyCode: 'KMF',
      population: '773407',
      capital: 'Moroni',
      continentName: 'Africa',
    },

    {
      countryCode: 'KP',
      countryName: 'North Korea',
      currencyCode: 'KPW',
      population: '22912177',
      capital: 'Pyongyang',
      continentName: 'Asia',
    },
    {
      countryCode: 'KR',
      countryName: 'South Korea',
      currencyCode: 'KRW',
      population: '48422644',
      capital: 'Seoul',
      continentName: 'Asia',
    },
    {
      countryCode: 'KW',
      countryName: 'Kuwait',
      currencyCode: 'KWD',
      population: '2789132',
      capital: 'Kuwait City',
      continentName: 'Asia',
    },
    {
      countryCode: 'KY',
      countryName: 'Cayman Islands',
      currencyCode: 'KYD',
      population: '44270',
      capital: 'George Town',
      continentName: 'North America',
    },
    {
      countryCode: 'KZ',
      countryName: 'Kazakhstan',
      currencyCode: 'KZT',
      population: '15340000',
      capital: 'Astana',
      continentName: 'Asia',
    },
    {
      countryCode: 'LA',
      countryName: 'Laos',
      currencyCode: 'LAK',
      population: '6368162',
      capital: 'Vientiane',
      continentName: 'Asia',
    },
    {
      countryCode: 'LB',
      countryName: 'Lebanon',
      currencyCode: 'LBP',
      population: '4125247',
      capital: 'Beirut',
      continentName: 'Asia',
    },

    {
      countryCode: 'LK',
      countryName: 'Sri Lanka',
      currencyCode: 'LKR',
      population: '21513990',
      capital: 'Colombo',
      continentName: 'Asia',
    },
    {
      countryCode: 'LR',
      countryName: 'Liberia',
      currencyCode: 'LRD',
      population: '3685076',
      capital: 'Monrovia',
      continentName: 'Africa',
    },
    {
      countryCode: 'LS',
      countryName: 'Lesotho',
      currencyCode: 'LSL',
      population: '1919552',
      capital: 'Maseru',
      continentName: 'Africa',
    },

    {
      countryCode: 'LY',
      countryName: 'Libya',
      currencyCode: 'LYD',
      population: '6461454',
      capital: 'Tripoli',
      continentName: 'Africa',
    },

    {
      countryCode: 'MD',
      countryName: 'Moldova',
      currencyCode: 'MDL',
      population: '4324000',
      capital: 'Chişinău',
      continentName: 'Europe',
    },

    {
      countryCode: 'MG',
      countryName: 'Madagascar',
      currencyCode: 'MGA',
      population: '21281844',
      capital: 'Antananarivo',
      continentName: 'Africa',
    },

    {
      countryCode: 'MK',
      countryName: 'Macedonia',
      currencyCode: 'MKD',
      population: '2062294',
      capital: 'Skopje',
      continentName: 'Europe',
    },

    {
      countryCode: 'MM',
      countryName: 'Myanmar [Burma]',
      currencyCode: 'MMK',
      population: '53414374',
      capital: 'Naypyitaw',
      continentName: 'Asia',
    },
    {
      countryCode: 'MN',
      countryName: 'Mongolia',
      currencyCode: 'MNT',
      population: '3086918',
      capital: 'Ulan Bator',
      continentName: 'Asia',
    },
    {
      countryCode: 'MO',
      countryName: 'Macao',
      currencyCode: 'MOP',
      population: '449198',
      capital: 'Macao',
      continentName: 'Asia',
    },

    {
      countryCode: 'MR',
      countryName: 'Mauritania',
      currencyCode: 'MRO',
      population: '3205060',
      capital: 'Nouakchott',
      continentName: 'Africa',
    },

    {
      countryCode: 'MU',
      countryName: 'Mauritius',
      currencyCode: 'MUR',
      population: '1294104',
      capital: 'Port Louis',
      continentName: 'Africa',
    },
    {
      countryCode: 'MV',
      countryName: 'Maldives',
      currencyCode: 'MVR',
      population: '395650',
      capital: 'Malé',
      continentName: 'Asia',
    },
    {
      countryCode: 'MW',
      countryName: 'Malawi',
      currencyCode: 'MWK',
      population: '15447500',
      capital: 'Lilongwe',
      continentName: 'Africa',
    },
    {
      countryCode: 'MX',
      countryName: 'Mexico',
      currencyCode: 'MXN',
      population: '112468855',
      capital: 'Mexico City',
      continentName: 'North America',
    },
    {
      countryCode: 'MY',
      countryName: 'Malaysia',
      currencyCode: 'MYR',
      population: '28274729',
      capital: 'Kuala Lumpur',
      continentName: 'Asia',
    },
    {
      countryCode: 'MZ',
      countryName: 'Mozambique',
      currencyCode: 'MZN',
      population: '22061451',
      capital: 'Maputo',
      continentName: 'Africa',
    },
    {
      countryCode: 'NA',
      countryName: 'Namibia',
      currencyCode: 'NAD',
      population: '2128471',
      capital: 'Windhoek',
      continentName: 'Africa',
    },
    {
      countryCode: 'NC',
      countryName: 'New Caledonia',
      currencyCode: 'XPF',
      population: '216494',
      capital: 'Noumea',
      continentName: 'Oceania',
    },

    {
      countryCode: 'NG',
      countryName: 'Nigeria',
      currencyCode: 'NGN',
      population: '154000000',
      capital: 'Abuja',
      continentName: 'Africa',
    },
    {
      countryCode: 'NI',
      countryName: 'Nicaragua',
      currencyCode: 'NIO',
      population: '5995928',
      capital: 'Managua',
      continentName: 'North America',
    },

    {
      countryCode: 'NP',
      countryName: 'Nepal',
      currencyCode: 'NPR',
      population: '28951852',
      capital: 'Kathmandu',
      continentName: 'Asia',
    },

    {
      countryCode: 'OM',
      countryName: 'Oman',
      currencyCode: 'OMR',
      population: '2967717',
      capital: 'Muscat',
      continentName: 'Asia',
    },
    {
      countryCode: 'PA',
      countryName: 'Panama',
      currencyCode: 'PAB',
      population: '3410676',
      capital: 'Panama City',
      continentName: 'North America',
    },
    {
      countryCode: 'PE',
      countryName: 'Peru',
      currencyCode: 'PEN',
      population: '29907003',
      capital: 'Lima',
      continentName: 'South America',
    },

    {
      countryCode: 'PG',
      countryName: 'Papua New Guinea',
      currencyCode: 'PGK',
      population: '6064515',
      capital: 'Port Moresby',
      continentName: 'Oceania',
    },
    {
      countryCode: 'PH',
      countryName: 'Philippines',
      currencyCode: 'PHP',
      population: '99900177',
      capital: 'Manila',
      continentName: 'Asia',
    },
    {
      countryCode: 'PK',
      countryName: 'Pakistan',
      currencyCode: 'PKR',
      population: '184404791',
      capital: 'Islamabad',
      continentName: 'Asia',
    },
    {
      countryCode: 'PL',
      countryName: 'Poland',
      currencyCode: 'PLN',
      population: '38500000',
      capital: 'Warsaw',
      continentName: 'Europe',
    },

    {
      countryCode: 'PY',
      countryName: 'Paraguay',
      currencyCode: 'PYG',
      population: '6375830',
      capital: 'Asunción',
      continentName: 'South America',
    },
    {
      countryCode: 'QA',
      countryName: 'Qatar',
      currencyCode: 'QAR',
      population: '840926',
      capital: 'Doha',
      continentName: 'Asia',
    },

    {
      countryCode: 'RO',
      countryName: 'Romania',
      currencyCode: 'RON',
      population: '21959278',
      capital: 'Bucharest',
      continentName: 'Europe',
    },
    {
      countryCode: 'RS',
      countryName: 'Serbia',
      currencyCode: 'RSD',
      population: '7344847',
      capital: 'Belgrade',
      continentName: 'Europe',
    },
    {
      countryCode: 'RU',
      countryName: 'Russia',
      currencyCode: 'RUB',
      population: '140702000',
      capital: 'Moscow',
      continentName: 'Europe',
    },
    {
      countryCode: 'RW',
      countryName: 'Rwanda',
      currencyCode: 'RWF',
      population: '11055976',
      capital: 'Kigali',
      continentName: 'Africa',
    },
    {
      countryCode: 'SA',
      countryName: 'Saudi Arabia',
      currencyCode: 'SAR',
      population: '25731776',
      capital: 'Riyadh',
      continentName: 'Asia',
    },
    {
      countryCode: 'SB',
      countryName: 'Solomon Islands',
      currencyCode: 'SBD',
      population: '559198',
      capital: 'Honiara',
      continentName: 'Oceania',
    },
    {
      countryCode: 'SC',
      countryName: 'Seychelles',
      currencyCode: 'SCR',
      population: '88340',
      capital: 'Victoria',
      continentName: 'Africa',
    },
    {
      countryCode: 'SD',
      countryName: 'Sudan',
      currencyCode: 'SDG',
      population: '35000000',
      capital: 'Khartoum',
      continentName: 'Africa',
    },
    {
      countryCode: 'SE',
      countryName: 'Sweden',
      currencyCode: 'SEK',
      population: '9828655',
      capital: 'Stockholm',
      continentName: 'Europe',
    },
    {
      countryCode: 'SG',
      countryName: 'Singapore',
      currencyCode: 'SGD',
      population: '4701069',
      capital: 'Singapore',
      continentName: 'Asia',
    },
    {
      countryCode: 'SH',
      countryName: 'Saint Helena',
      currencyCode: 'SHP',
      population: '7460',
      capital: 'Jamestown',
      continentName: 'Africa',
    },

    {
      countryCode: 'SL',
      countryName: 'Sierra Leone',
      currencyCode: 'SLL',
      population: '5245695',
      capital: 'Freetown',
      continentName: 'Africa',
    },

    {
      countryCode: 'SO',
      countryName: 'Somalia',
      currencyCode: 'SOS',
      population: '10112453',
      capital: 'Mogadishu',
      continentName: 'Africa',
    },
    {
      countryCode: 'SR',
      countryName: 'Suriname',
      currencyCode: 'SRD',
      population: '492829',
      capital: 'Paramaribo',
      continentName: 'South America',
    },
    {
      countryCode: 'SS',
      countryName: 'South Sudan',
      currencyCode: 'SSP',
      population: '8260490',
      capital: 'Juba',
      continentName: 'Africa',
    },
    {
      countryCode: 'ST',
      countryName: 'São Tomé and Príncipe',
      currencyCode: 'STD',
      population: '175808',
      capital: 'São Tomé',
      continentName: 'Africa',
    },

    {
      countryCode: 'SY',
      countryName: 'Syria',
      currencyCode: 'SYP',
      population: '22198110',
      capital: 'Damascus',
      continentName: 'Asia',
    },
    {
      countryCode: 'SZ',
      countryName: 'Swaziland',
      currencyCode: 'SZL',
      population: '1354051',
      capital: 'Mbabane',
      continentName: 'Africa',
    },

    {
      countryCode: 'TH',
      countryName: 'Thailand',
      currencyCode: 'THB',
      population: '67089500',
      capital: 'Bangkok',
      continentName: 'Asia',
    },
    {
      countryCode: 'TJ',
      countryName: 'Tajikistan',
      currencyCode: 'TJS',
      population: '7487489',
      capital: 'Dushanbe',
      continentName: 'Asia',
    },

    {
      countryCode: 'TM',
      countryName: 'Turkmenistan',
      currencyCode: 'TMT',
      population: '4940916',
      capital: 'Ashgabat',
      continentName: 'Asia',
    },
    {
      countryCode: 'TN',
      countryName: 'Tunisia',
      currencyCode: 'TND',
      population: '10589025',
      capital: 'Tunis',
      continentName: 'Africa',
    },
    {
      countryCode: 'TO',
      countryName: 'Tonga',
      currencyCode: 'TOP',
      population: '122580',
      capital: "Nuku'alofa",
      continentName: 'Oceania',
    },
    {
      countryCode: 'TR',
      countryName: 'Turkey',
      currencyCode: 'TRY',
      population: '77804122',
      capital: 'Ankara',
      continentName: 'Asia',
    },
    {
      countryCode: 'TT',
      countryName: 'Trinidad and Tobago',
      currencyCode: 'TTD',
      population: '1228691',
      capital: 'Port of Spain',
      continentName: 'North America',
    },

    {
      countryCode: 'TW',
      countryName: 'Taiwan',
      currencyCode: 'TWD',
      population: '22894384',
      capital: 'Taipei',
      continentName: 'Asia',
    },
    {
      countryCode: 'TZ',
      countryName: 'Tanzania',
      currencyCode: 'TZS',
      population: '41892895',
      capital: 'Dodoma',
      continentName: 'Africa',
    },
    {
      countryCode: 'UA',
      countryName: 'Ukraine',
      currencyCode: 'UAH',
      population: '45415596',
      capital: 'Kiev',
      continentName: 'Europe',
    },
    {
      countryCode: 'UG',
      countryName: 'Uganda',
      currencyCode: 'UGX',
      population: '33398682',
      capital: 'Kampala',
      continentName: 'Africa',
    },
    {
      countryCode: 'AD',
      countryName: 'Andorra',
      currencyCode: 'USD',
      population: '84000',
      capital: 'Andorra la Vella',
      continentName: 'Europe',
    },

    {
      countryCode: 'UY',
      countryName: 'Uruguay',
      currencyCode: 'UYU',
      population: '3477000',
      capital: 'Montevideo',
      continentName: 'South America',
    },
    {
      countryCode: 'UZ',
      countryName: 'Uzbekistan',
      currencyCode: 'UZS',
      population: '27865738',
      capital: 'Tashkent',
      continentName: 'Asia',
    },

    {
      countryCode: 'VE',
      countryName: 'Venezuela',
      currencyCode: 'VEF',
      population: '27223228',
      capital: 'Caracas',
      continentName: 'South America',
    },

    {
      countryCode: 'VN',
      countryName: 'Vietnam',
      currencyCode: 'VND',
      population: '89571130',
      capital: 'Hanoi',
      continentName: 'Asia',
    },
    {
      countryCode: 'VU',
      countryName: 'Vanuatu',
      currencyCode: 'VUV',
      population: '221552',
      capital: 'Port Vila',
      continentName: 'Oceania',
    },

    {
      countryCode: 'WS',
      countryName: 'Samoa',
      currencyCode: 'WST',
      population: '192001',
      capital: 'Apia',
      continentName: 'Oceania',
    },

    {
      countryCode: 'YE',
      countryName: 'Yemen',
      currencyCode: 'YER',
      population: '23495361',
      capital: 'Sanaa',
      continentName: 'Asia',
    },

    {
      countryCode: 'ZA',
      countryName: 'South Africa',
      currencyCode: 'ZAR',
      population: '49000000',
      capital: 'Pretoria',
      continentName: 'Africa',
    },
    {
      countryCode: 'ZM',
      countryName: 'Zambia',
      currencyCode: 'ZMW',
      population: '13460305',
      capital: 'Lusaka',
      continentName: 'Africa',
    },
    {
      countryCode: 'ZW',
      countryName: 'Zimbabwe',
      currencyCode: 'ZWL',
      population: '13061000',
      capital: 'Harare',
      continentName: 'Africa',
    },
  ];

  ngOnInit(): void {
    this.caseId = this.route.snapshot.params['caseId'];
  //  console.log(this.caseId);
    this.getCaseApiCall();

    let data: any = localStorage.getItem('users');
    this.userData = JSON.parse(data);

    let str = this.route.snapshot.params['caseId'];
    this.bannerCaseName = str.substring(str.lastIndexOf(' ') + 1);

    //========borrow details form validation================
    this.firstFormGroup = this._formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      contactNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      emailAddress: [
        '',
        [
          Validators.required,
          Validators.email
          //Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      countryName: [{ value: '', disabled: false }, Validators.required],
      zipCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      cityName: [{ value: '', disabled: false }, Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      isCoApplicantCked: [false],
      coapplicantfirstName: [''],
      coapplicantlastName: [''],
      coapplicantcontactNumber: [''],
      coapplicantemailAddress: [''],
      coapplicantcountryName: [{ value: '', disabled: false }],
      coapplicantzipCode: [''],
      coapplicantcityName: [{ value: '', disabled: false }],
      coapplicantaddressLine1: [''],
      coapplicantaddressLine2: [''],
    });

    //========employee details form validation================

    this.secondFormGroup = this._formBuilder.group({
      employeeType: [
        '',
        [Validators.required], //Validators.pattern('^[a-zA-Z]+$')
      ],
      employerName: [
        '',
        [Validators.required], //Validators.pattern('^[a-zA-Z]+$')
      ],
      position: ['', [Validators.required]], // Validators.pattern('^[a-zA-Z]+$')]
      countryName: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      cityName: ['', [Validators.required]],
      pinCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      contactNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      addressLine1: ['', [Validators.required]],

      coapplicant_employeeType: [''],
      coapplicant_employerName: [''],
      coapplicant_position: [''],
      coapplicant_countryName: [''],
      coapplicant_startDate: [''],
      coapplicant_cityName: [''],
      coapplicant_pinCode: [''],
      coapplicant_contactNumber: [''],
      coapplicant_addressLine1: [''],
    });

    this.coapplicantFormGroup = this._formBuilder.group({
      // ssnumber: [
      //   '',
      //   [
      //     Validators.required,
      //     Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      //     Validators.pattern('.*\\S.*[a-zA-z0-9 ]'),
      //   ],
      // ],
      ssnumber: ['', [Validators.required]],
    });

    //for form array
    this.productForm = this._formBuilder.group({
      //  name: [{value: 'assets', disabled: true}],
      totalIncome: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      quantities: this._formBuilder.array([]),
      monthlies: this._formBuilder.array([]),
      recurries: this._formBuilder.array([]),
      liabilities: this._formBuilder.array([]),
      // ==========co-applicant ============
      coapplicant_totalIncome: [''],
      coapplicant_quantities: this._formBuilder.array([]),
      coapplicant_monthlies: this._formBuilder.array([]),
      coapplicant_recurries: this._formBuilder.array([]),
      coapplicant_liabilities: this._formBuilder.array([]),
    });

    //================== if checkbox is checked or unchecked validation ====================
    this.firstFormGroup
      .get('isCoApplicantCked')
      ?.valueChanges.subscribe((checkedValue) => {
        //========== co-borrow ================
        const coapplicantfirstName = this.firstFormGroup.get(
          'coapplicantfirstName'
        );
        const coapplicantlastName = this.firstFormGroup.get(
          'coapplicantlastName'
        );
        const coapplicantcontactNumber = this.firstFormGroup.get(
          'coapplicantcontactNumber'
        );
        const coapplicantemailAddress = this.firstFormGroup.get(
          'coapplicantemailAddress'
        );
        const coapplicantcountryName = this.firstFormGroup.get(
          'coapplicantcountryName'
        );
        const coapplicantzipCode =
          this.firstFormGroup.get('coapplicantzipCode');
        const coapplicantcityName = this.firstFormGroup.get(
          'coapplicantcityName'
        );
        const coapplicantaddressLine1 = this.firstFormGroup.get(
          'coapplicantaddressLine1'
        );

        //========== co-employee ================
        const coapplicant_employeeType = this.secondFormGroup.get(
          'coapplicant_employeeType'
        );
        const coapplicant_employerName = this.secondFormGroup.get(
          'coapplicant_employerName'
        );
        const coapplicant_position = this.secondFormGroup.get(
          'coapplicant_position'
        );
        const coapplicant_countryName = this.secondFormGroup.get(
          'coapplicant_countryName'
        );
        const coapplicant_startDate = this.secondFormGroup.get(
          'coapplicant_startDate'
        );
        const coapplicant_cityName = this.secondFormGroup.get(
          'coapplicant_cityName'
        );
        const coapplicant_pinCode = this.secondFormGroup.get(
          'coapplicant_pinCode'
        );
        const coapplicant_contactNumber = this.secondFormGroup.get(
          'coapplicant_contactNumber'
        );
        const coapplicant_addressLine1 = this.secondFormGroup.get(
          'coapplicant_addressLine1'
        );

        //========== co-Financial ================
        const coapplicant_totalIncome = this.productForm.get(
          'coapplicant_totalIncome'
        );

        if (checkedValue) {
          //============== co-brower============
          coapplicantfirstName?.setValidators([
            Validators.required,
            Validators.pattern('^[a-zA-Z]+$'),
          ]);
          coapplicantlastName?.setValidators([
            Validators.required,
            Validators.pattern('^[a-zA-Z]+$'),
          ]);
          coapplicantcontactNumber?.setValidators([
            Validators.required,
            Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$'),
          ]);
          coapplicantemailAddress?.setValidators([
            Validators.required,
            Validators.email
            //Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
          ]);
          coapplicantcountryName?.setValidators(Validators.required);
          coapplicantzipCode?.setValidators([
            Validators.required,
            Validators.pattern('^[1-9][0-9]{5}$'),
          ]);
          coapplicantcityName?.setValidators(Validators.required);
          coapplicantaddressLine1?.setValidators(Validators.required);

          //============== co-employee============
          coapplicant_employeeType?.setValidators(Validators.required);
          coapplicant_employerName?.setValidators(Validators.required);
          coapplicant_position?.setValidators(Validators.required);
          coapplicant_countryName?.setValidators(Validators.required);
          coapplicant_startDate?.setValidators(Validators.required);
          coapplicant_cityName?.setValidators(Validators.required);
          coapplicant_pinCode?.setValidators([
            Validators.required,
            Validators.pattern('^[1-9][0-9]{5}$'),
          ]);
          coapplicant_contactNumber?.setValidators([
            Validators.required,
            Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$'),
          ]);
          coapplicant_addressLine1?.setValidators(Validators.required);

          //=========== co-financial=================
          coapplicant_totalIncome?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
        } else {
          coapplicantfirstName?.clearValidators();
          coapplicantlastName?.clearValidators();
          coapplicantcontactNumber?.clearValidators();
          coapplicantemailAddress?.clearValidators();
          coapplicantcountryName?.clearValidators();
          coapplicantzipCode?.clearValidators();
          coapplicantcityName?.clearValidators();
          coapplicantaddressLine1?.clearValidators();

          coapplicant_employeeType?.clearValidators();
          coapplicant_employerName?.clearValidators();
          coapplicant_position?.clearValidators();
          coapplicant_countryName?.clearValidators();
          coapplicant_startDate?.clearValidators();
          coapplicant_cityName?.clearValidators();
          coapplicant_pinCode?.clearValidators();
          coapplicant_contactNumber?.clearValidators();
          coapplicant_addressLine1?.clearValidators();

          coapplicant_totalIncome?.clearValidators();
        }

        coapplicantfirstName?.updateValueAndValidity();
        coapplicantlastName?.updateValueAndValidity();
        coapplicantcontactNumber?.updateValueAndValidity();
        coapplicantemailAddress?.updateValueAndValidity();
        coapplicantcountryName?.updateValueAndValidity();
        coapplicantzipCode?.updateValueAndValidity();
        coapplicantcityName?.updateValueAndValidity();
        coapplicantaddressLine1?.updateValueAndValidity();

        coapplicant_employeeType?.updateValueAndValidity();
        coapplicant_employerName?.updateValueAndValidity();
        coapplicant_position?.updateValueAndValidity();
        coapplicant_countryName?.updateValueAndValidity();
        coapplicant_startDate?.updateValueAndValidity();
        coapplicant_cityName?.updateValueAndValidity();
        coapplicant_pinCode?.updateValueAndValidity();
        coapplicant_contactNumber?.updateValueAndValidity();
        coapplicant_addressLine1?.updateValueAndValidity();

        coapplicant_totalIncome?.updateValueAndValidity();
      });

    //================== Property FormGroup==================
    this.propertyFormGroup = this._formBuilder.group({
      countryName: ['', Validators.required],
      zipCode: [
        '',
        [Validators.required, Validators.pattern('^[1-9][0-9]{5}$')],
      ],
      cityName: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      PropertyType: ['', [Validators.required]],
      propertyStatus: ['', [Validators.required]],
      propertyPrice: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      propertyCurrency: ['', [Validators.required]],
      exsitingPropertyChecked: [''],
      existingpropertyType: [''],
      existingpropertyContracts: [''],
      existingpropertyAddress1: [''],
      existingpropertyCityName: [''],
      existingpropertyCountryName: [''],
      existingpropertyZipCode: [''],
      existingpropertyAddress2: [''],
      existingpropertyPrice: [''],
      existingpropertyCurrency: [''],
      arrangePropertyChecked: [false],
      saleAmount: [''],
      saleCurrency: [''],
      transferDate: [''],
    });

    //================== if checkbox is checked or unchecked validation ====================
    this.propertyFormGroup
      .get('arrangePropertyChecked')
      ?.valueChanges.subscribe((checkedVal) => {
        const saleAmount = this.propertyFormGroup.get('saleAmount');
        const saleCurrency = this.propertyFormGroup.get('saleCurrency');
        const transferDate = this.propertyFormGroup.get('transferDate');

        if (checkedVal) {
          saleCurrency?.setValidators(Validators.required);
          transferDate?.setValidators(Validators.required);
          saleAmount?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
        } else {
          saleCurrency?.clearValidators();
          transferDate?.clearValidators();
          saleAmount?.clearValidators();
        }

        saleCurrency?.updateValueAndValidity();
        transferDate?.updateValueAndValidity();
        saleAmount?.updateValueAndValidity();
      });

    //================== Loan FormGroup==================
    this.loanFormGroup = this._formBuilder.group({
      loanPurpose: ['', [Validators.required]],
      productType: ['', [Validators.required]],
      productScope: [''],
      checkMortgage: [false],
      mortgageLoanAmount: [''],
      mortgageLoanCurrency: [''],
      mortgageLoanSelfContAmount: [''],
      mortgageLoanSelfContCurrency: [''],
      mortgageTimePeriod: [''],
      checkBridge: [false],
      bridgeLoanAmount: [''],
      bridgeLoanAmountCurrency: [''],
      bridgeLoanDate: [''],
      bridgeTimePeriodinMonth: [''],
      downpaymentrequired: [false],
      downpayLoanAmount: [''],
      downpayLoanAmountCurrency: [''],
      downpaydate: [''],
      downpaymentTimePeriodinYear: [''],
      creditCheck: [''],
    });

    //================== if checkbox is checked or unchecked validation ====================
    this.loanFormGroup
      .get('checkMortgage')
      ?.valueChanges.subscribe((checkedVal) => {
        const mortgageLoanAmount = this.loanFormGroup.get('mortgageLoanAmount');
        const mortgageLoanCurrency = this.loanFormGroup.get(
          'mortgageLoanCurrency'
        );
        const mortgageLoanSelfContCurrency = this.loanFormGroup.get(
          'mortgageLoanSelfContCurrency'
        );
        const mortgageLoanSelfContAmount = this.loanFormGroup.get(
          'mortgageLoanSelfContAmount'
        );
        const mortgageTimePeriod = this.loanFormGroup.get('mortgageTimePeriod');

        if (checkedVal) {
          mortgageLoanCurrency?.setValidators(Validators.required);
          mortgageLoanSelfContCurrency?.setValidators(Validators.required);
          mortgageLoanAmount?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
          mortgageLoanSelfContAmount?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
          mortgageTimePeriod?.setValidators(Validators.required);
        } else {
          mortgageLoanCurrency?.clearValidators();
          mortgageLoanSelfContCurrency?.clearValidators();
          mortgageLoanAmount?.clearValidators();
          mortgageLoanSelfContAmount?.clearValidators();
          mortgageTimePeriod?.clearValidators();
        }

        mortgageLoanCurrency?.updateValueAndValidity();
        mortgageLoanSelfContCurrency?.updateValueAndValidity();
        mortgageLoanAmount?.updateValueAndValidity();
        mortgageLoanSelfContAmount?.updateValueAndValidity();
        mortgageTimePeriod?.updateValueAndValidity();
      });

    this.loanFormGroup
      .get('checkBridge')
      ?.valueChanges.subscribe((checkedVal) => {
        const bridgeLoanAmount = this.loanFormGroup.get('bridgeLoanAmount');
        const bridgeLoanAmountCurrency = this.loanFormGroup.get(
          'bridgeLoanAmountCurrency'
        );
        const bridgeLoanDate = this.loanFormGroup.get('bridgeLoanDate');
        const bridgeTimePeriodinMonth = this.loanFormGroup.get(
          'bridgeTimePeriodinMonth'
        );

        if (checkedVal) {
          bridgeLoanAmountCurrency?.setValidators(Validators.required);
          bridgeLoanDate?.setValidators([Validators.required]);
          bridgeLoanAmount?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
          bridgeTimePeriodinMonth?.setValidators(Validators.required);
        } else {
          bridgeLoanAmountCurrency?.clearValidators();
          bridgeLoanDate?.clearValidators();
          bridgeLoanAmount?.clearValidators();
          bridgeTimePeriodinMonth?.clearValidators();
        }

        bridgeLoanAmountCurrency?.updateValueAndValidity();
        bridgeLoanDate?.updateValueAndValidity();
        bridgeLoanAmount?.updateValueAndValidity();
        bridgeTimePeriodinMonth?.updateValueAndValidity();
      });

    this.loanFormGroup
      .get('downpaymentrequired')
      ?.valueChanges.subscribe((checkedVal) => {
        const downpayLoanAmount = this.loanFormGroup.get('downpayLoanAmount');
        const downpayLoanAmountCurrency = this.loanFormGroup.get(
          'downpayLoanAmountCurrency'
        );
        const downpaydate = this.loanFormGroup.get('downpaydate');
        const downpaymentTimePeriodinYear = this.loanFormGroup.get(
          'downpaymentTimePeriodinYear'
        );

        if (checkedVal) {
          downpayLoanAmountCurrency?.setValidators(Validators.required);
          downpaydate?.setValidators([Validators.required]);
          downpayLoanAmount?.setValidators([
            Validators.required,
            Validators.pattern(/^\d+(\.\d{1,2})?$/),
          ]);
          downpaymentTimePeriodinYear?.setValidators(Validators.required);
        } else {
          downpayLoanAmountCurrency?.clearValidators();
          downpaydate?.clearValidators();
          downpayLoanAmount?.clearValidators();
          downpaymentTimePeriodinYear?.clearValidators();
        }

        downpayLoanAmountCurrency?.updateValueAndValidity();
        downpaydate?.updateValueAndValidity();
        downpayLoanAmount?.updateValueAndValidity();
        downpaymentTimePeriodinYear?.updateValueAndValidity();
      });

    //================= Upload Document Details =================
    //for form array
    this.uploadFormGroup = this._formBuilder.group({
      //  name: [{value: 'assets', disabled: true}],
      uploadDocumetRowItems: this._formBuilder.array([]),
    });
  }

  goPreviousStep(n: number) {
    this.step = n;
    setTimeout(() => (this.stepper.selectedIndex = n - 1));
  }

  getCaseApiCall() {
    /** spinner starts on init */
   // this.spinner.show();
    this.caseService.getCaseById(this.caseId).subscribe(
      (data) => {
        console.log(data);

        if (data.status === 200) {
          localStorage.removeItem('etag');
          localStorage.setItem('etag', data.headers.get('etag'));
          // this.demoData = data;
          this.viewTemp = true;
          if (data.body.content.KYCColour === 'Green') {
            this.greenRedFlag = true;
            this.kycVerifyData = 'Green';
          } else {
            this.greenRedFlag = false;
            this.kycVerifyData = 'Gray';
          }

          if (data.body.content.pxCurrentStage != 'PRIM1') {
            this.nxtBtnShowafterPM = true;
          } else {
            this.nxtBtnShowafterPM = false;
          }

          this.viewData = data.body.content.pyWorkParty;
          this.assetsDatas = data.body.content.pyWorkParty.Customer.Assets;
          this.monthllyDatas =
            data.body.content.pyWorkParty.Customer.IncomeList;
          this.recurrDatas =
            data.body.content.pyWorkParty.Customer.ExpensesList;
          this.liabilDatas =
            data.body.content.pyWorkParty.Customer.LiabilitiesList;
          this.viewLoanData = data.body.content;
          this.viewPropertyData = data.body.content.PropertyDetails;

          this.setBorrowerValue(data);
          this.setEmployeeDetailsValue(data);
          this.patchResultList(data);
          this.propertySetValue(data);
          this.setLoanDetailValue(data);

          // setTimeout(() => {
          //   /** spinner ends after 5 seconds */
          //   this.spinner.hide();
          // }, 1000);
        }
      },
      (error) => {
        console.log(error);
        this.notificationService.showError(
          `${error.message}`,
          'error Occuring'
        );
      }
    );
  }

  get coapplicant() {
    return this.coapplicantFormGroup.controls;
  }
  get personaldetil() {
    return this.firstFormGroup.controls;
  }
  get employeedetil() {
    return this.secondFormGroup.controls;
  }
  get finnacialdetil() {
    return this.productForm.controls;
  }
  get propertydetil() {
    return this.propertyFormGroup.controls;
  }
  get documentdetil() {
    return this.uploadFormGroup.controls;
  }
  get loanDetail() {
    return this.loanFormGroup.controls;
  }

  // ===========================Start Borrower Details ===============================

  setBorrowerValue(data: any) {
    // this.caseService.getCaseById(this.caseId).subscribe(data=>{

    if (data.body === null) {
      //if (data.body.content.CoApplicantCheck==="true") {
      this.firstFormGroup.patchValue({
        firstName: '',
        lastName: '',
        contactNumber: '',
        emailAddress: '',
        zipCode: '',
        cityName: '',
        addressLine1: '',
        addressLine2: '',
        isCoApplicantCked: false,

        coapplicantfirstName: '',
        coapplicantlastName: '',
        coapplicantcontactNumber: '',
        coapplicantemailAddress: '',
        coapplicantcountryName: '',
        coapplicantzipCode: '',
        coapplicantcityName: '',
        coapplicantaddressLine1: '',
        coapplicantaddressLine2: '',
      });
    } else {
      if (data.body.content.pxCurrentStage != 'PRIM1') {
        if (data.body.content.KYCColour === 'Green') {
          this.greenRedFlag = true;
        } else {
          this.greenRedFlag = false;
        }
        this.selectDisable = true;
        this.nxtBtnShow = true;
        this.isLinear = false;

        this.firstFormGroup = this._formBuilder.group({
          firstName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.FirstName === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.FirstName === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.FirstName,
              disabled: true,
            },
            Validators.required,
          ],
          lastName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.LastName === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.LastName === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.LastName,
              disabled: true,
            },
            Validators.required,
          ],
          contactNumber: [
            {
              value:
                data.body.content.pyWorkParty.Customer.pyMobilePhone ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyMobilePhone === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyMobilePhone,
              disabled: true,
            },
            Validators.required,
          ],
          emailAddress: [
            {
              value:
                data.body.content.pyWorkParty.Customer.pyEmail1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyEmail1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyEmail1,
              disabled: true,
            },
            Validators.required,
          ],
          countryName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.Address.Home.Country ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .Country === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home.Country,
              disabled: true,
            },
            [Validators.required],
          ],
          zipCode: [
            {
              value:
                data.body.content.pyWorkParty.Customer.Address.Home.ZipCode ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .ZipCode === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home.ZipCode,
              disabled: true,
            },
            Validators.required,
          ],
          cityName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.Address.Home.City ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home.City ===
                    ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home.City,
              disabled: true,
            },
            Validators.required,
          ],
          addressLine1: [
            {
              value:
                data.body.content.pyWorkParty.Customer.Address.Home
                  .AddressLine1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .AddressLine1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .AddressLine1,
              disabled: true,
            },
            Validators.required,
          ],
          addressLine2: [
            {
              value:
                data.body.content.pyWorkParty.Customer.Address.Home
                  .AddressLine2 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .AddressLine2 === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.Address.Home
                      .AddressLine2,
              disabled: true,
            },
          ],
          isCoApplicantCked: {
            value: data.body.content.CoApplicantCheck === 'true' ? true : false,
            disabled: this.isDisable,
          },
          coapplicantfirstName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.FirstName ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.FirstName === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.FirstName,
              disabled: true,
            },
          ],
          coapplicantlastName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.LastName === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.LastName === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.LastName,
              disabled: true,
            },
          ],
          coapplicantcontactNumber: [
            {
              value:
                data.body.content.pyWorkParty.Customer.pyMobilePhone ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyMobilePhone === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyMobilePhone,
              disabled: true,
            },
          ],
          coapplicantemailAddress: [
            {
              value:
                data.body.content.pyWorkParty.Customer.pyEmail1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyEmail1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.pyEmail1,
              disabled: true,
            },
          ],
          coapplicantcountryName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.Address.Home
                  .Country === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .Country === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .Country,
              disabled: true,
            },
          ],
          coapplicantzipCode: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.Address.Home
                  .ZipCode === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .ZipCode === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .ZipCode,
              disabled: true,
            },
          ],
          coapplicantcityName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.Address.Home.City ===
                undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .City === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home.City,
              disabled: true,
            },
          ],
          coapplicantaddressLine1: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.Address.Home
                  .AddressLine1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .AddressLine1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .AddressLine1,
              disabled: true,
            },
          ],
          coapplicantaddressLine2: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.Address.Home
                  .AddressLine2 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .AddressLine2 === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant.Address.Home
                      .AddressLine2,
              disabled: true,
            },
          ],
        });
        this.coapplicantFormGroup = this._formBuilder.group({
          ssnumber: [{ value: 890102 - 3287, disabled: true }],
        });
        if (data.body.content.CoApplicantCheck === 'true') {
          this.isCheckedApplicant = true;
          this.isCheckedApplicantData = true;
          this.coAppliciantEmployeeDetails = true;
          this.coAppliciantFinancialDetails = true;
        } else {
          this.isCheckedApplicant = false;
          this.isCheckedApplicantData = false;
          this.coAppliciantEmployeeDetails = false;
          this.coAppliciantFinancialDetails = false;
        }
      } else {
        if (data.body.content.CoApplicantCheck === 'true') {
          this.firstFormGroup.patchValue({
            firstName:
              data.body.content.pyWorkParty.Customer.FirstName === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.FirstName === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.FirstName,
            lastName:
              data.body.content.pyWorkParty.Customer.LastName === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.LastName === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.LastName,
            contactNumber:
              data.body.content.pyWorkParty.Customer.pyMobilePhone === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.pyMobilePhone === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.pyMobilePhone,
            emailAddress:
              data.body.content.pyWorkParty.Customer.pyEmail1 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.pyEmail1 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.pyEmail1,
            countryName:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .Country === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .Country === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.Country,
            zipCode:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .ZipCode === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .ZipCode === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.ZipCode,
            cityName:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City ===
                  ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City,
            addressLine1:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1,
            addressLine2:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2,
            isCoApplicantCked: true,

            coapplicantfirstName:
              data.body.content.pyWorkParty.CoApplicant.FirstName === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.FirstName === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.FirstName,
            coapplicantlastName:
              data.body.content.pyWorkParty.CoApplicant.LastName === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.LastName === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.LastName,
            coapplicantcontactNumber:
              data.body.content.pyWorkParty.CoApplicant.pyMobilePhone ===
              undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyMobilePhone === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyMobilePhone,
            coapplicantemailAddress:
              data.body.content.pyWorkParty.CoApplicant.pyEmail1 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyEmail1 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyEmail1,
            coapplicantcountryName:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country,
            coapplicantzipCode:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode,
            coapplicantcityName:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .City === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .City === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home.City,
            coapplicantaddressLine1:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1,
            coapplicantaddressLine2:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2,
          });

          this.coapplicantFormGroup.patchValue({
            ssnumber: '890102-3287',
          });
          this.isCheckedApplicant = true;
          this.isCheckedApplicantData = true;
          this.coAppliciantEmployeeDetails = true;
          this.coAppliciantFinancialDetails = true;
        } else {
          this.firstFormGroup.patchValue({
            firstName:
              data.body.content.pyWorkParty.Customer.FirstName === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.FirstName === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.FirstName,
            lastName:
              data.body.content.pyWorkParty.Customer.LastName === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.LastName === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.LastName,
            contactNumber:
              data.body.content.pyWorkParty.Customer.pyMobilePhone === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.pyMobilePhone === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.pyMobilePhone,
            emailAddress:
              data.body.content.pyWorkParty.Customer.pyEmail1 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.pyEmail1 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.pyEmail1,
            countryName:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .Country === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .Country === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.Country,
            zipCode:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .ZipCode === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .ZipCode === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.ZipCode,
            cityName:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City ===
                  ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home.City,
            addressLine1:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine1,
            addressLine2:
              data.body.content.pyWorkParty.Customer.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2 === undefined
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2 === ''
                ? ''
                : data.body.content.pyWorkParty.Customer.Address.Home
                    .AddressLine2,
            isCoApplicantCked: false,

            coapplicantfirstName:
              data.body.content.pyWorkParty.CoApplicant.FirstName === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.FirstName === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.FirstName,
            coapplicantlastName:
              data.body.content.pyWorkParty.CoApplicant.LastName === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.LastName === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.LastName,
            coapplicantcontactNumber:
              data.body.content.pyWorkParty.CoApplicant.pyMobilePhone ===
              undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyMobilePhone === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyMobilePhone,
            coapplicantemailAddress:
              data.body.content.pyWorkParty.CoApplicant.pyEmail1 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyEmail1 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.pyEmail1,
            coapplicantcountryName:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .Country,
            coapplicantzipCode:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .ZipCode,
            coapplicantcityName:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .City === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .City === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home.City,
            coapplicantaddressLine1:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine1,
            coapplicantaddressLine2:
              data.body.content.pyWorkParty.CoApplicant.Address === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home ===
                  undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2 === undefined
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2 === ''
                ? ''
                : data.body.content.pyWorkParty.CoApplicant.Address.Home
                    .AddressLine2,
          });

          this.coapplicantFormGroup.patchValue({
            ssnumber: '890102-3287',
          });
          this.isCheckedApplicant = false;
          this.isCheckedApplicantData = false;
          this.coAppliciantEmployeeDetails = false;
          this.coAppliciantFinancialDetails = false;
        }
      }
    }

    // })
  }

  getCoApplicantData() {
    this.coapplicantSubmitted = true;
    if (this.coapplicantFormGroup.invalid) {
      this.isCheckedApplicantData = false;
      this.coAppliciantEmployeeDetails = false;
      this.coAppliciantFinancialDetails = false;
      return;
    } else {
      this.isCheckedApplicantData = true;
      this.saveBtn = false;
      this.coAppliciantEmployeeDetails = true;
      this.coAppliciantFinancialDetails = true;
    }
  }

  borrowerDetailsData(num: number) {
    this.browwerdetailSubmitted = true;

    if (this.firstFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );

              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        //   this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }

  // =========================== End Borrower Details ===============================

  // =========================== start Employee Details ===============================
  private formatDate(date: any) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  private formatDateGMT(date: any) {
    if (date === '') {
      return '';
    } else {
      const d = new Date(date);

      let month = '' + (d.getMonth() + 1);
      let day = '' + d.getDate();
      const year = d.getFullYear();
      let datetext = 'T000000.000 GMT';
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return year + month + day + datetext;
    }
  }

  setEmployeeDetailsValue(data: any) {
    if (data.body === null) {
      this.secondFormGroup.patchValue({
        employeeType: '',
        employerName: '',
        position: '',
        countryName: '',
        startDate: '',
        cityName: '',
        pinCode: '',
        contactNumber: '',
        addressLine1: '',
        coapplicant_employeeType: '',
        coapplicant_employerName: '',
        coapplicant_position: '',
        coapplicant_countryName: '',
        coapplicant_startDate: '',
        coapplicant_cityName: '',
        coapplicant_pinCode: '',
        coapplicant_contactNumber: '',
        coapplicant_addressLine1: '',
      });
    } else {
      if (data.body.content.pxCurrentStage != 'PRIM1') {
        this.secondFormGroup = this._formBuilder.group({
          employeeType: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .TypeOfEmployment === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .TypeOfEmployment === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .TypeOfEmployment,
              disabled: true,
            },
          ],
          employerName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .EmployerName === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .EmployerName === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .EmployerName,
              disabled: true,
            },
          ],
          position: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .Title === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Title === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Title,
              disabled: true,
            },
          ],
          countryName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .Address.Country === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.Country === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.Country,
              disabled: true,
            },
          ],
          startDate: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .WorkStartDate === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .WorkStartDate === ''
                  ? ''
                  : this.formatDate(
                      data.body.content.pyWorkParty.Customer
                        .EmploymentHistory[0].WorkStartDate
                    ),
              disabled: true,
            },
          ],
          cityName: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .Address.City === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.City === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.City,
              disabled: true,
            },
          ],
          pinCode: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .Address.ZipCode === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.ZipCode === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.ZipCode,
              disabled: true,
            },
          ],
          contactNumber: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .EmployerWorkPhone === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .EmployerWorkPhone === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .EmployerWorkPhone,
              disabled: true,
            },
          ],
          addressLine1: [
            {
              value:
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .Address.AddressLine1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.AddressLine1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                      .Address.AddressLine1,
              disabled: true,
            },
          ],

          coapplicant_employeeType: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .TypeOfEmployment === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].TypeOfEmployment === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].TypeOfEmployment,
              disabled: true,
            },
          ],
          coapplicant_employerName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .EmployerName === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].EmployerName === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].EmployerName,
              disabled: true,
            },
          ],
          coapplicant_position: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .Title === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Title === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Title,
              disabled: true,
            },
          ],
          coapplicant_countryName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .Address.Country === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.Country === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.Country,
              disabled: true,
            },
          ],
          coapplicant_startDate: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .WorkStartDate === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].WorkStartDate === ''
                  ? ''
                  : this.formatDate(
                      data.body.content.pyWorkParty.CoApplicant
                        .EmploymentHistory[0].WorkStartDate
                    ),
              disabled: true,
            },
          ],
          coapplicant_cityName: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .Address.City === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.City === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.City,
              disabled: true,
            },
          ],
          coapplicant_pinCode: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .Address.ZipCode === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.ZipCode === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.ZipCode,
              disabled: true,
            },
          ],
          coapplicant_contactNumber: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .EmployerWorkPhone === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].EmployerWorkPhone === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].EmployerWorkPhone,
              disabled: true,
            },
          ],
          coapplicant_addressLine1: [
            {
              value:
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .Address.AddressLine1 === undefined
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.AddressLine1 === ''
                  ? ''
                  : data.body.content.pyWorkParty.CoApplicant
                      .EmploymentHistory[0].Address.AddressLine1,
              disabled: true,
            },
          ],
        });
      }

      this.secondFormGroup.patchValue({
        employeeType:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .TypeOfEmployment === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .TypeOfEmployment === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .TypeOfEmployment,
        employerName:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerName === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerName === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerName,
        position:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Title === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Title === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0].Title,
        countryName:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.Country === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.Country === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.Country,
        startDate:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .WorkStartDate === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .WorkStartDate === ''
            ? ''
            : this.formatDate(
                data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                  .WorkStartDate
              ),
        cityName:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.City === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.City === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.City,
        pinCode:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.ZipCode === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.ZipCode === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.ZipCode,
        contactNumber:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerWorkPhone === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerWorkPhone === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .EmployerWorkPhone,
        addressLine1:
          data.body.content.pyWorkParty.Customer.EmploymentHistory === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.AddressLine1 === undefined
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.AddressLine1 === ''
            ? ''
            : data.body.content.pyWorkParty.Customer.EmploymentHistory[0]
                .Address.AddressLine1,

        coapplicant_employeeType:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .TypeOfEmployment === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .TypeOfEmployment === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .TypeOfEmployment,
        coapplicant_employerName:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerName === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerName === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerName,
        coapplicant_position:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Title === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Title === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Title,
        coapplicant_countryName:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.Country === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.Country === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.Country,
        coapplicant_startDate:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .WorkStartDate === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .WorkStartDate === ''
            ? ''
            : this.formatDate(
                data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                  .WorkStartDate
              ),
        coapplicant_cityName:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.City === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.City === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.City,
        coapplicant_pinCode:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.ZipCode === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.ZipCode === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.ZipCode,
        coapplicant_contactNumber:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerWorkPhone === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerWorkPhone === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .EmployerWorkPhone,
        coapplicant_addressLine1:
          data.body.content.pyWorkParty.CoApplicant.EmploymentHistory ===
          undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.AddressLine1 === undefined
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.AddressLine1 === ''
            ? ''
            : data.body.content.pyWorkParty.CoApplicant.EmploymentHistory[0]
                .Address.AddressLine1,
      });
    }
  }

  employeeDetailsData(num: number) {
    this.employeedetailSubmitted = true;

    if (this.secondFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              this.step = num;

              setTimeout(() => (this.stepper.selectedIndex = num - 1));
              // this.router.navigateByUrl('/consumer/dashboard');
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }
  // =========================== End Personal Details ===============================

  //=========== Start Finnacial Details =======================
  // this method patches FormArray
  patchResultList(dat: any) {
    let assetDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.Customer.Assets === undefined
        ? ''
        : dat.body.content.pyWorkParty.Customer.Assets === ''
        ? ''
        : dat.body.content.pyWorkParty.Customer.Assets;
    let monthlyDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.Customer.IncomeList === undefined
        ? ''
        : dat.body.content.pyWorkParty.Customer.IncomeList === ''
        ? ''
        : dat.body.content.pyWorkParty.Customer.IncomeList;
    let recurringDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.Customer.ExpensesList === undefined
        ? ''
        : dat.body.content.pyWorkParty.Customer.ExpensesList === ''
        ? ''
        : dat.body.content.pyWorkParty.Customer.ExpensesList;
    let liabilitDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.Customer.LiabilitiesList === undefined
        ? ''
        : dat.body.content.pyWorkParty.Customer.LiabilitiesList === ''
        ? ''
        : dat.body.content.pyWorkParty.Customer.LiabilitiesList;

    const quantityformArray = new FormArray([]);
    const monthformArray = new FormArray([]);
    const recurringformArray = new FormArray([]);
    const liabilityformArray = new FormArray([]);

    //================= co applicant===============
    let cassetDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.Assets === undefined
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.Assets === ''
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.Assets;
    let cmonthlyDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.IncomeList === undefined
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.IncomeList === ''
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.IncomeList;
    let crecurringDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.ExpensesList === undefined
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.ExpensesList === ''
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.ExpensesList;
    let cliabilitDatas =
      dat.body === null
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.LiabilitiesList === undefined
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.LiabilitiesList === ''
        ? ''
        : dat.body.content.pyWorkParty.CoApplicant.LiabilitiesList;

    const cquantityformArray = new FormArray([]);
    const cmonthformArray = new FormArray([]);
    const crecurringformArray = new FormArray([]);
    const cliabilityformArray = new FormArray([]);

    if (dat.body == null) {
      this.productForm.patchValue({
        totalIncome: 0,
        coapplicant_totalIncome: 0,
        quantities: [],
        monthlies: [],
        recurries: [],
        liabilities: [],
        coapplicant_quantities: [],
        coapplicant_monthlies: [],
        coapplicant_recurries: [],
        coapplicant_liabilities: [],
      });
    } else {
      if (dat.body.content.pxCurrentStage != 'PRIM1') {
        this.productForm = this._formBuilder.group({
          totalIncome: [
            {
              disabled: true,
              value:
                dat.body.content.pyWorkParty.Customer.Income.BaseEmplIncome ===
                ''
                  ? 0
                  : dat.body.content.pyWorkParty.Customer.Income
                      .BaseEmplIncome === undefined
                  ? 0
                  : parseFloat(
                      dat.body.content.pyWorkParty.Customer.Income
                        .BaseEmplIncome
                    ),
            },
          ],
          coapplicant_totalIncome: [
            {
              value:
                dat.body.content.pyWorkParty.CoApplicant.Income
                  .BaseEmplIncome === ''
                  ? 0
                  : dat.body.content.pyWorkParty.CoApplicant.Income
                      .BaseEmplIncome === undefined
                  ? 0
                  : parseFloat(
                      dat.body.content.pyWorkParty.CoApplicant.Income
                        .BaseEmplIncome
                    ),
              disabled: true,
            },
          ],
          quantities: quantityformArray,
          monthlies: monthformArray,
          recurries: recurringformArray,
          liabilities: liabilityformArray,
          coapplicant_quantities: cquantityformArray,
          coapplicant_monthlies: cmonthformArray,
          coapplicant_recurries: crecurringformArray,
          coapplicant_liabilities: cliabilityformArray,
        });
      }

      this.productForm.patchValue({
        totalIncome:
          dat.body.content.pyWorkParty.Customer.Income.BaseEmplIncome === ''
            ? 0
            : dat.body.content.pyWorkParty.Customer.Income.BaseEmplIncome ===
              undefined
            ? 0
            : parseFloat(
                dat.body.content.pyWorkParty.Customer.Income.BaseEmplIncome
              ),
        coapplicant_totalIncome:
          dat.body.content.pyWorkParty.CoApplicant.Income.BaseEmplIncome === ''
            ? 0
            : dat.body.content.pyWorkParty.CoApplicant.Income.BaseEmplIncome ===
              undefined
            ? 0
            : parseFloat(
                dat.body.content.pyWorkParty.CoApplicant.Income.BaseEmplIncome
              ),
      });

      this.productForm.setControl('quantities', quantityformArray);
      this.productForm.setControl('monthlies', monthformArray);
      this.productForm.setControl('recurries', recurringformArray);
      this.productForm.setControl('liabilities', liabilityformArray);
      this.productForm.setControl('coapplicant_quantities', cquantityformArray);
      this.productForm.setControl('coapplicant_monthlies', cmonthformArray);
      this.productForm.setControl('coapplicant_recurries', crecurringformArray);
      this.productForm.setControl(
        'coapplicant_liabilities',
        cliabilityformArray
      );

      //================assets===========

      if (assetDatas.length === 0) {
        return;
      } else {
        assetDatas.forEach((x: any) => {
          quantityformArray.push(
            this._formBuilder.group({
              AssetType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.AssetType, disabled: true }]
                  : [{ value: x.AssetType, disabled: false }],
              OriginalValue:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.OriginalValue, disabled: true }]
                  : [{ value: x.OriginalValue, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================monthly===========
      if (monthlyDatas.length === 0) {
        return;
      } else {
        monthlyDatas.forEach((x: any) => {
          monthformArray.push(
            this._formBuilder.group({
              IncomeType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.IncomeType, disabled: true }]
                  : [{ value: x.IncomeType, disabled: false }],
              IncomeValue:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.IncomeValue, disabled: true }]
                  : [{ value: x.IncomeValue, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================recurring===========
      if (recurringDatas.length === 0) {
        return;
      } else {
        recurringDatas.forEach((x: any) => {
          recurringformArray.push(
            this._formBuilder.group({
              ExpenseType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.ExpenseType, disabled: true }]
                  : [{ value: x.ExpenseType, disabled: false }],
              ExpenseAmount:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.ExpenseAmount, disabled: true }]
                  : [{ value: x.ExpenseAmount, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================liabilities===========
      if (liabilitDatas.length === 0) {
        return;
      } else {
        liabilitDatas.forEach((x: any) => {
          liabilityformArray.push(
            this._formBuilder.group({
              LiabilityType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.LiabilityType, disabled: true }]
                  : [{ value: x.LiabilityType, disabled: false }],
              LiabilityAmount:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.LiabilityAmount, disabled: true }]
                  : [{ value: x.LiabilityAmount, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //=================== co-applicant assets ============
      if (cassetDatas.length === 0) {
        return;
      } else {
        cassetDatas.forEach((x: any) => {
          cquantityformArray.push(
            this._formBuilder.group({
              AssetType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.AssetType, disabled: true }]
                  : [{ value: x.AssetType, disabled: false }],
              OriginalValue:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.OriginalValue, disabled: true }]
                  : [{ value: x.OriginalValue, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================co-applicant monthly===========
      if (cmonthlyDatas.length === 0) {
        return;
      } else {
        cmonthlyDatas.forEach((x: any) => {
          cmonthformArray.push(
            this._formBuilder.group({
              IncomeType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.IncomeType, disabled: true }]
                  : [{ value: x.IncomeType, disabled: false }],
              IncomeValue:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.IncomeValue, disabled: true }]
                  : [{ value: x.IncomeValue, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================co-applicant recurring===========
      if (crecurringDatas.length === 0) {
        return;
      } else {
        crecurringDatas.forEach((x: any) => {
          crecurringformArray.push(
            this._formBuilder.group({
              ExpenseType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.ExpenseType, disabled: true }]
                  : [{ value: x.ExpenseType, disabled: false }],
              ExpenseAmount:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.ExpenseAmount, disabled: true }]
                  : [{ value: x.ExpenseAmount, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }

      //================co-applicant liabilities===========
      if (cliabilitDatas.length === 0) {
        return;
      } else {
        cliabilitDatas.forEach((x: any) => {
          cliabilityformArray.push(
            this._formBuilder.group({
              LiabilityType:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.LiabilityType, disabled: true }]
                  : [{ value: x.LiabilityType, disabled: false }],
              LiabilityAmount:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.LiabilityAmount, disabled: true }]
                  : [{ value: x.LiabilityAmount, disabled: false }],
              CurrencyCodesList:
                dat.body.content.pxCurrentStage != 'PRIM1'
                  ? [{ value: x.CurrencyCodesList, disabled: true }]
                  : [{ value: x.CurrencyCodesList, disabled: false }],
            })
          );
        });
      }
    }
  }

  quantities(): FormArray {
    return this.productForm.get('quantities') as FormArray;
  }
  monthlies(): FormArray {
    return this.productForm.get('monthlies') as FormArray;
  }
  recurries(): FormArray {
    return this.productForm.get('recurries') as FormArray;
  }
  liabilities(): FormArray {
    return this.productForm.get('liabilities') as FormArray;
  }

  newQuantity(): FormGroup {
    return this._formBuilder.group({
      AssetType: ['', Validators.required],
      OriginalValue: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newMonthly(): FormGroup {
    return this._formBuilder.group({
      IncomeType: ['', Validators.required],
      IncomeValue: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newRecurring(): FormGroup {
    return this._formBuilder.group({
      ExpenseType: ['', Validators.required],
      ExpenseAmount: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newLiberity(): FormGroup {
    return this._formBuilder.group({
      LiabilityType: ['', Validators.required],
      LiabilityAmount: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }

  addCommonQuantity(val: any) {
    if (val === 'month') {
      this.monthlies().push(this.newMonthly());
    } else if (val === 'recur') {
      this.recurries().push(this.newRecurring());
    } else if (val === 'liabilities') {
      this.liabilities().push(this.newLiberity());
    } else {
      this.quantities().push(this.newQuantity());
    }
  }

  commonRemoveQuantity(i: number, val: any) {
    if (val === 'assets') {
      this.quantities().removeAt(i);
      let qutit = this.productForm.get('quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.assetTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'monthly') {
      this.monthlies().removeAt(i);
      let month = this.productForm.get('monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.monthlyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'recurries') {
      this.recurries().removeAt(i);
      let recurr = this.productForm.get('recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.recurringTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else {
      this.liabilities().removeAt(i);
      let liabilit = this.productForm.get('liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.libertyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    }
    this.totalIncomeCumser =
      this.assetTotalSumAmount +
      this.monthlyTotalSumAmount;
      //this.recurringTotalSumAmount;
    this.totalIncomeCumserExp =this.recurringTotalSumAmount + this.libertyTotalSumAmount;
  }

  onKeypressEvent(event: any, i: number, val: any) {
    if (val === 'assets') {
      let qutit = this.productForm.get('quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.assetTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'month') {
      let month = this.productForm.get('monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.monthlyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else if (val === 'recur') {
      let recurr = this.productForm.get('recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.recurringTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    } else {
      let liabilit = this.productForm.get('liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.libertyTotalSumAmount = arts.reduce(function (a: any, b: any) {
        return a + b;
      }, 0);
    }

    this.totalIncomeCumser =
      this.assetTotalSumAmount +
      this.monthlyTotalSumAmount ;
      //this.recurringTotalSumAmount;
    this.totalIncomeCumserExp =this.recurringTotalSumAmount + this.libertyTotalSumAmount;
  }

  //================ co-applicant ================

  coapplicant_quantities(): FormArray {
    return this.productForm.get('coapplicant_quantities') as FormArray;
  }
  coapplicant_monthlies(): FormArray {
    return this.productForm.get('coapplicant_monthlies') as FormArray;
  }
  coapplicant_recurries(): FormArray {
    return this.productForm.get('coapplicant_recurries') as FormArray;
  }
  coapplicant_liabilities(): FormArray {
    return this.productForm.get('coapplicant_liabilities') as FormArray;
  }

  newcoapplicant_Quantity(): FormGroup {
    return this._formBuilder.group({
      AssetType: ['', Validators.required],
      OriginalValue: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newcoapplicant_Monthly(): FormGroup {
    return this._formBuilder.group({
      IncomeType: ['', Validators.required],
      IncomeValue: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newcoapplicant_Recurring(): FormGroup {
    return this._formBuilder.group({
      ExpenseType: ['', Validators.required],
      ExpenseAmount: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }
  newcoapplicant_Liberity(): FormGroup {
    return this._formBuilder.group({
      LiabilityType: ['', Validators.required],
      LiabilityAmount: [
        '',
        [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)],
      ],
      CurrencyCodesList: ['', Validators.required],
    });
  }

  coapplicant_addCommonQuantity(val: any) {
    if (val === 'month') {
      this.coapplicant_monthlies().push(this.newcoapplicant_Monthly());
    } else if (val === 'recur') {
      this.coapplicant_recurries().push(this.newcoapplicant_Recurring());
    } else if (val === 'liabilities') {
      this.coapplicant_liabilities().push(this.newcoapplicant_Liberity());
    } else {
      this.coapplicant_quantities().push(this.newcoapplicant_Quantity());
    }
  }

  coapplicant_commonRemoveQuantity(i: number, val: any) {
    if (val === 'assets') {
      this.coapplicant_quantities().removeAt(i);
      let qutit = this.productForm.get('coapplicant_quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.coapplicant_assetTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'monthly') {
      this.coapplicant_monthlies().removeAt(i);
      let month = this.productForm.get('coapplicant_monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.coapplicant_monthlyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'recurries') {
      this.coapplicant_recurries().removeAt(i);
      let recurr = this.productForm.get('coapplicant_recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.coapplicant_recurringTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else {
      this.coapplicant_liabilities().removeAt(i);
      let liabilit = this.productForm.get('coapplicant_liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.coapplicant_libertyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    }

    this.totalIncomecoApplicant =
      this.coapplicant_assetTotalSumAmount +
      this.coapplicant_monthlyTotalSumAmount ;
      //this.coapplicant_recurringTotalSumAmount;
    this.totalExpensiveCoApplicant =this.coapplicant_recurringTotalSumAmount + this.coapplicant_libertyTotalSumAmount;
  }

  coapplicant_onKeypressEvent(event: any, i: number, val: any) {
    if (val === 'assets') {
      let qutit = this.productForm.get('coapplicant_quantities')?.value;
      let arts: any[] = [];
      qutit.forEach((el: any) => {
        arts.push(parseFloat(el.OriginalValue));
      });

      this.coapplicant_assetTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'month') {
      let month = this.productForm.get('coapplicant_monthlies')?.value;
      let arts: any[] = [];
      month.forEach((el: any) => {
        arts.push(parseFloat(el.IncomeValue));
      });
      this.coapplicant_monthlyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else if (val === 'recur') {
      let recurr = this.productForm.get('coapplicant_recurries')?.value;
      let arts: any[] = [];
      recurr.forEach((el: any) => {
        arts.push(parseFloat(el.ExpenseAmount));
      });
      this.coapplicant_recurringTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    } else {
      let liabilit = this.productForm.get('coapplicant_liabilities')?.value;
      let arts: any[] = [];
      liabilit.forEach((el: any) => {
        arts.push(parseFloat(el.LiabilityAmount));
      });
      this.coapplicant_libertyTotalSumAmount = arts.reduce(function (
        a: any,
        b: any
      ) {
        return a + b;
      },
      0);
    }
    this.totalIncomecoApplicant =
      this.coapplicant_assetTotalSumAmount +
      this.coapplicant_monthlyTotalSumAmount ;
      //this.coapplicant_recurringTotalSumAmount;
    this.totalExpensiveCoApplicant = this.coapplicant_recurringTotalSumAmount + this.coapplicant_libertyTotalSumAmount;
  }

  sym = '';
  symblChange(event: any, i: number) {
   // console.log(event.value);
    this.sym = event.value;
    
  }
  coapplicant_symblChange(event: any) {
   // console.log(event.target.value);
  }

  cst = 0;
  ccst = 0;
  onBlur(event: any, val: any) {
    //console.log(this.productForm.get('totalIncome')?.value)
    this.cst =
      this.productForm.get('totalIncome')?.value
        ? parseFloat(this.productForm.get('totalIncome')?.value)
        : 0;
    this.ccst =
      this.productForm.get('coapplicant_totalIncome')?.value
        ? parseFloat(this.productForm.get('coapplicant_totalIncome')?.value)
        : 0;
  }

  onSubmit(num: number) {
    this.finnacialdetailSubmitted = true;
    if (this.productForm.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }

  //=========== End Finnacial Details =======================

  //======================== Property Details ==========================

  propertySetValue(data: any) {
    if (data.body == null || data.body.content.PropertyDetails === undefined) {
      this.propertyFormGroup.patchValue({
        countryName: '',
        zipCode: '',
        cityName: '',
        addressLine1: '',
        addressLine2: '',
        PropertyType: '',
        propertyStatus: '',
        propertyPrice: 0,
        propertyCurrency: '',
        exsitingPropertyChecked: false,
        existingpropertyType: '',
        existingpropertyContracts: '',
        existingpropertyCityName: '',
        existingpropertyCountryName: '',
        existingpropertyZipCode: '',
        existingpropertyAddress1: '',
        existingpropertyAddress2: '',
        existingpropertyPrice: '',
        existingpropertyCurrency: '',
        arrangePropertyChecked: false,
        saleAmount: 0,
        saleCurrency: '',
        transferDate: '',
      });
      this.existpropertyDetailsData = false;
      this.arrangepropertyDetailsData = false;
    } else {
      if (data.body.content.pxCurrentStage != 'PRIM1') {
        this.propertyFormGroup = this._formBuilder.group({
          countryName: [
            {
              value:
                data.body.content.PropertyDetails.Address.Country === undefined
                  ? ''
                  : data.body.content.PropertyDetails.Address.Country === ''
                  ? ''
                  : data.body.content.PropertyDetails.Address.Country,
              disabled: true,
            },
          ],
          zipCode: [
            {
              value:
                data.body.content.PropertyDetails.Address.ZipCode === undefined
                  ? ''
                  : data.body.content.PropertyDetails.Address.ZipCode === ''
                  ? ''
                  : data.body.content.PropertyDetails.Address.ZipCode,
              disabled: true,
            },
          ],
          cityName: [
            {
              value:
                data.body.content.PropertyDetails.Address.City === undefined
                  ? ''
                  : data.body.content.PropertyDetails.Address.City === ''
                  ? ''
                  : data.body.content.PropertyDetails.Address.City,
              disabled: true,
            },
          ],
          addressLine1: [
            {
              value:
                data.body.content.PropertyDetails.Address.AddressLine1 ===
                undefined
                  ? ''
                  : data.body.content.PropertyDetails.Address.AddressLine1 ===
                    ''
                  ? ''
                  : data.body.content.PropertyDetails.Address.AddressLine1,
              disabled: true,
            },
          ],
          addressLine2: [
            {
              value:
                data.body.content.PropertyDetails.Address.AddressLine2 ===
                undefined
                  ? ''
                  : data.body.content.PropertyDetails.Address.AddressLine2 ===
                    ''
                  ? ''
                  : data.body.content.PropertyDetails.Address.AddressLine2,
              disabled: true,
            },
          ],
          PropertyType: [
            {
              value:
                data.body.content.PropertyDetails.PropertyType === undefined
                  ? ''
                  : data.body.content.PropertyDetails.PropertyType === ''
                  ? ''
                  : data.body.content.PropertyDetails.PropertyType,
              disabled: true,
            },
          ],
          propertyStatus: [
            {
              value:
                data.body.content.PropertyDetails.PropertyStatus === undefined
                  ? ''
                  : data.body.content.PropertyDetails.PropertyStatus === ''
                  ? ''
                  : data.body.content.PropertyDetails.PropertyStatus,
              disabled: true,
            },
          ],
          propertyPrice: [
            {
              value:
                data.body.content.PropertyDetails.PropertyPrice === undefined
                  ? 0
                  : data.body.content.PropertyDetails.PropertyPrice === ''
                  ? 0
                  : data.body.content.PropertyDetails.PropertyPrice,
              disabled: true,
            },
          ],
          propertyCurrency: [
            {
              value:
                data.body.content.PropertyDetails.CurrencyCodesList ===
                undefined
                  ? ''
                  : data.body.content.PropertyDetails.CurrencyCodesList === ''
                  ? ''
                  : data.body.content.PropertyDetails.CurrencyCodesList,
              disabled: true,
            },
          ],
          exsitingPropertyChecked: [
            {
              value:
                data.body.content.PropertyDetails.HasExistingProperty === 'true'
                  ? true
                  : false,
              disabled: true,
            },
          ],
          existingpropertyType: [
            {
              value:
                data.body.content.ExistingProperty.PropertyType === undefined
                  ? ''
                  : data.body.content.ExistingProperty.PropertyType === ''
                  ? ''
                  : data.body.content.ExistingProperty.PropertyType,

              disabled: true,
            },
          ],
          existingpropertyContracts: [
            {
              value:
                data.body.content.ExistingProperty.SignedContractForSale ===
                undefined
                  ? 'No'
                  : data.body.content.ExistingProperty.SignedContractForSale ===
                    ''
                  ? 'No'
                  : data.body.content.ExistingProperty.SignedContractForSale,
              disabled: true,
            },
          ],
          existingpropertyCityName: [
            {
              value:
                data.body.content.ExistingProperty.Address.City === undefined
                  ? ''
                  : data.body.content.ExistingProperty.Address.City === ''
                  ? ''
                  : data.body.content.ExistingProperty.Address.City,
              disabled: true,
            },
          ],
          existingpropertyCountryName: [
            {
              value:
                data.body.content.ExistingProperty.Address.Country === undefined
                  ? ''
                  : data.body.content.ExistingProperty.Address.Country === ''
                  ? ''
                  : data.body.content.ExistingProperty.Address.Country,
              disabled: true,
            },
          ],
          existingpropertyZipCode: [
            {
              value:
                data.body.content.ExistingProperty.Address.ZipCode === undefined
                  ? ''
                  : data.body.content.ExistingProperty.Address.ZipCode === ''
                  ? ''
                  : data.body.content.ExistingProperty.Address.ZipCode,
              disabled: true,
            },
          ],
          existingpropertyAddress1: [
            {
              value:
                data.body.content.ExistingProperty.Address.AddressLine1 ===
                undefined
                  ? ''
                  : data.body.content.ExistingProperty.Address.AddressLine1 ===
                    ''
                  ? ''
                  : data.body.content.ExistingProperty.Address.AddressLine1,
              disabled: true,
            },
          ],
          existingpropertyAddress2: [
            {
              value:
                data.body.content.ExistingProperty.Address.AddressLine2 ===
                undefined
                  ? ''
                  : data.body.content.ExistingProperty.Address.AddressLine2 ===
                    ''
                  ? ''
                  : data.body.content.ExistingProperty.Address.AddressLine2,
              disabled: true,
            },
          ],
          existingpropertyPrice: [
            {
              value:
                data.body.content.ExistingProperty.PropertyPrice === undefined
                  ? 0
                  : data.body.content.ExistingProperty.PropertyPrice === ''
                  ? 0
                  : data.body.content.ExistingProperty.PropertyPrice,
              disabled: true,
            },
          ],
          existingpropertyCurrency: [
            {
              value:
                data.body.content.ExistingProperty.CurrencyCodesList ===
                undefined
                  ? ''
                  : data.body.content.ExistingProperty.CurrencyCodesList === ''
                  ? ''
                  : data.body.content.ExistingProperty.CurrencyCodesList,
              disabled: true,
            },
          ],
          arrangePropertyChecked: [
            {
              value:
                data.body.content.ExistingProperty.IsSold === 'true'
                  ? true
                  : false,
              disabled: true,
            },
          ],
          saleAmount: [
            {
              value:
                data.body.content.ExistingProperty.SalesAmount === undefined
                  ? 0
                  : data.body.content.ExistingProperty.SalesAmount === ''
                  ? 0
                  : data.body.content.ExistingProperty.SalesAmount,
              disabled: true,
            },
          ],
          saleCurrency: [
            {
              value:
                data.body.content.ExistingProperty.CurrencyCodesList ===
                undefined
                  ? ''
                  : data.body.content.ExistingProperty.CurrencyCodesList === ''
                  ? ''
                  : data.body.content.ExistingProperty.CurrencyCodesList,
              disabled: true,
            },
          ],
          transferDate: [
            {
              value:
                data.body.content.ExistingProperty.TransferDate === undefined
                  ? ''
                  : data.body.content.ExistingProperty.TransferDate === ''
                  ? ''
                  : this.formatDate(
                      data.body.content.ExistingProperty.TransferDate
                    ),
              disabled: true,
            },
          ],
        });

        if (
          data.body.content.PropertyDetails.HasExistingProperty === 'true' &&
          data.body.content.ExistingProperty.IsSold === 'true'
        ) {
          this.existpropertyDetailsData = true;
          this.arrangepropertyDetailsData = true;
        } else if (
          data.body.content.PropertyDetails.HasExistingProperty === 'true' &&
          data.body.content.ExistingProperty.IsSold === 'false'
        ) {
          this.existpropertyDetailsData = true;
          this.arrangepropertyDetailsData = false;
        } else {
          this.existpropertyDetailsData = false;
          this.arrangepropertyDetailsData = false;
        }
      }

      if (
        data.body.content.PropertyDetails.HasExistingProperty === 'true' &&
        data.body.content.ExistingProperty.IsSold === 'true'
      ) {
        this.propertyFormGroup.patchValue({
          countryName:
            data.body.content.PropertyDetails.Address.Country === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.Country === ''
              ? ''
              : data.body.content.PropertyDetails.Address.Country,
          zipCode:
            data.body.content.PropertyDetails.Address.ZipCode === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode === ''
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode,
          cityName:
            data.body.content.PropertyDetails.Address.City === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.City === ''
              ? ''
              : data.body.content.PropertyDetails.Address.City,
          addressLine1:
            data.body.content.PropertyDetails.Address.AddressLine1 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1,
          addressLine2:
            data.body.content.PropertyDetails.Address.AddressLine2 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2,
          PropertyType:
            data.body.content.PropertyDetails.PropertyType === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyType === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyType,
          propertyStatus:
            data.body.content.PropertyDetails.PropertyStatus === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus,
          propertyPrice:
            data.body.content.PropertyDetails.PropertyPrice === undefined
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice === ''
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice,
          propertyCurrency:
            data.body.content.PropertyDetails.CurrencyCodesList === undefined
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList === ''
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList,
          exsitingPropertyChecked: true,
          existingpropertyType:
            data.body.content.ExistingProperty.PropertyType === undefined
              ? ''
              : data.body.content.ExistingProperty.PropertyType === ''
              ? ''
              : data.body.content.ExistingProperty.PropertyType,
          existingpropertyContracts:
            data.body.content.ExistingProperty.SignedContractForSale ===
            undefined
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale === ''
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale,
          existingpropertyCityName:
            data.body.content.ExistingProperty.Address.City === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.City === ''
              ? ''
              : data.body.content.ExistingProperty.Address.City,
          existingpropertyCountryName:
            data.body.content.ExistingProperty.Address.Country === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.Country === ''
              ? ''
              : data.body.content.ExistingProperty.Address.Country,
          existingpropertyZipCode:
            data.body.content.ExistingProperty.Address.ZipCode === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode === ''
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode,
          existingpropertyAddress1:
            data.body.content.ExistingProperty.Address.AddressLine1 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1,
          existingpropertyAddress2:
            data.body.content.ExistingProperty.Address.AddressLine2 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2,
          existingpropertyPrice:
            data.body.content.ExistingProperty.PropertyPrice === undefined
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice === ''
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice,
          existingpropertyCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,
          arrangePropertyChecked: true,
          saleAmount:
            data.body.content.ExistingProperty.SalesAmount === undefined
              ? 0
              : data.body.content.ExistingProperty.SalesAmount === ''
              ? 0
              : data.body.content.ExistingProperty.SalesAmount,
          saleCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,
          transferDate:
            data.body.content.ExistingProperty.TransferDate === undefined
              ? ''
              : data.body.content.ExistingProperty.TransferDate === ''
              ? ''
              : this.formatDate(
                  data.body.content.ExistingProperty.TransferDate
                ),
        });
        this.existpropertyDetailsData = true;
        this.arrangepropertyDetailsData = true;
      } else if (
        data.body.content.PropertyDetails.HasExistingProperty === 'true' &&
        data.body.content.ExistingProperty.IsSold === 'false'
      ) {
        this.propertyFormGroup.patchValue({
          countryName:
            data.body.content.PropertyDetails.Address.Country === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.Country === ''
              ? ''
              : data.body.content.PropertyDetails.Address.Country,
          zipCode:
            data.body.content.PropertyDetails.Address.ZipCode === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode === ''
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode,
          cityName:
            data.body.content.PropertyDetails.Address.City === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.City === ''
              ? ''
              : data.body.content.PropertyDetails.Address.City,
          addressLine1:
            data.body.content.PropertyDetails.Address.AddressLine1 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1,
          addressLine2:
            data.body.content.PropertyDetails.Address.AddressLine2 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2,
          PropertyType:
            data.body.content.PropertyDetails.PropertyType === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyType === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyType,
          propertyStatus:
            data.body.content.PropertyDetails.PropertyStatus === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus,
          propertyPrice:
            data.body.content.PropertyDetails.PropertyPrice === undefined
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice === ''
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice,
          propertyCurrency:
            data.body.content.PropertyDetails.CurrencyCodesList === undefined
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList === ''
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList,

          exsitingPropertyChecked: true,
          existingpropertyType:
            data.body.content.ExistingProperty.PropertyType === undefined
              ? ''
              : data.body.content.ExistingProperty.PropertyType === ''
              ? ''
              : data.body.content.ExistingProperty.PropertyType,
          existingpropertyContracts:
            data.body.content.ExistingProperty.SignedContractForSale ===
            undefined
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale === ''
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale,
          existingpropertyCityName:
            data.body.content.ExistingProperty.Address.City === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.City === ''
              ? ''
              : data.body.content.ExistingProperty.Address.City,
          existingpropertyCountryName:
            data.body.content.ExistingProperty.Address.Country === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.Country === ''
              ? ''
              : data.body.content.ExistingProperty.Address.Country,
          existingpropertyZipCode:
            data.body.content.ExistingProperty.Address.ZipCode === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode === ''
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode,
          existingpropertyAddress1:
            data.body.content.ExistingProperty.Address.AddressLine1 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1,
          existingpropertyAddress2:
            data.body.content.ExistingProperty.Address.AddressLine2 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2,
          existingpropertyPrice:
            data.body.content.ExistingProperty.PropertyPrice === undefined
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice === ''
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice,
          existingpropertyCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,

          arrangePropertyChecked: false,
          saleAmount:
            data.body.content.ExistingProperty.SalesAmount === undefined
              ? 0
              : data.body.content.ExistingProperty.SalesAmount === ''
              ? 0
              : data.body.content.ExistingProperty.SalesAmount,
          saleCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,
          transferDate:
            data.body.content.ExistingProperty.TransferDate === undefined
              ? ''
              : data.body.content.ExistingProperty.TransferDate === ''
              ? ''
              : this.formatDate(
                  data.body.content.ExistingProperty.TransferDate
                ),
        });
        this.existpropertyDetailsData = true;
        this.arrangepropertyDetailsData = false;
      } else {
        this.propertyFormGroup.patchValue({
          countryName:
            data.body.content.PropertyDetails.Address.Country === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.Country === ''
              ? ''
              : data.body.content.PropertyDetails.Address.Country,
          zipCode:
            data.body.content.PropertyDetails.Address.ZipCode === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode === ''
              ? ''
              : data.body.content.PropertyDetails.Address.ZipCode,
          cityName:
            data.body.content.PropertyDetails.Address.City === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.City === ''
              ? ''
              : data.body.content.PropertyDetails.Address.City,
          addressLine1:
            data.body.content.PropertyDetails.Address.AddressLine1 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine1,
          addressLine2:
            data.body.content.PropertyDetails.Address.AddressLine2 === undefined
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2 === ''
              ? ''
              : data.body.content.PropertyDetails.Address.AddressLine2,
          PropertyType:
            data.body.content.PropertyDetails.PropertyType === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyType === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyType,
          propertyStatus:
            data.body.content.PropertyDetails.PropertyStatus === undefined
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus === ''
              ? ''
              : data.body.content.PropertyDetails.PropertyStatus,
          propertyPrice:
            data.body.content.PropertyDetails.PropertyPrice === undefined
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice === ''
              ? 0
              : data.body.content.PropertyDetails.PropertyPrice,
          propertyCurrency:
            data.body.content.PropertyDetails.CurrencyCodesList === undefined
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList === ''
              ? ''
              : data.body.content.PropertyDetails.CurrencyCodesList,

          exsitingPropertyChecked:
            data.body.content.PropertyDetails.HasExistingProperty === ''
              ? false
              : data.body.content.PropertyDetails.HasExistingProperty ===
                undefined
              ? false
              : JSON.parse(
                  data.body.content.PropertyDetails.HasExistingProperty
                ),

          existingpropertyType:
            data.body.content.ExistingProperty.PropertyType === undefined
              ? ''
              : data.body.content.ExistingProperty.PropertyType === ''
              ? ''
              : data.body.content.ExistingProperty.PropertyType,
          existingpropertyContracts:
            data.body.content.ExistingProperty.SignedContractForSale ===
            undefined
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale === ''
              ? 'No'
              : data.body.content.ExistingProperty.SignedContractForSale,
          existingpropertyCityName:
            data.body.content.ExistingProperty.Address.City === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.City === ''
              ? ''
              : data.body.content.ExistingProperty.Address.City,
          existingpropertyCountryName:
            data.body.content.ExistingProperty.Address.Country === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.Country === ''
              ? ''
              : data.body.content.ExistingProperty.Address.Country,
          existingpropertyZipCode:
            data.body.content.ExistingProperty.Address.ZipCode === undefined
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode === ''
              ? ''
              : data.body.content.ExistingProperty.Address.ZipCode,
          existingpropertyAddress1:
            data.body.content.ExistingProperty.Address.AddressLine1 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine1,
          existingpropertyAddress2:
            data.body.content.ExistingProperty.Address.AddressLine2 ===
            undefined
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2 === ''
              ? ''
              : data.body.content.ExistingProperty.Address.AddressLine2,
          existingpropertyPrice:
            data.body.content.ExistingProperty.PropertyPrice === undefined
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice === ''
              ? 0
              : data.body.content.ExistingProperty.PropertyPrice,
          existingpropertyCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,
          arrangePropertyChecked:
            data.body.content.ExistingProperty.IsSold === ''
              ? false
              : data.body.content.ExistingProperty.IsSold === undefined
              ? false
              : JSON.parse(data.body.content.ExistingProperty.IsSold),

          saleAmount:
            data.body.content.ExistingProperty.SalesAmount === undefined
              ? 0
              : data.body.content.ExistingProperty.SalesAmount === ''
              ? 0
              : data.body.content.ExistingProperty.SalesAmount,
          saleCurrency:
            data.body.content.ExistingProperty.CurrencyCodesList === undefined
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList === ''
              ? ''
              : data.body.content.ExistingProperty.CurrencyCodesList,
          transferDate:
            data.body.content.ExistingProperty.TransferDate === undefined
              ? ''
              : data.body.content.ExistingProperty.TransferDate === ''
              ? ''
              : this.formatDate(
                  data.body.content.ExistingProperty.TransferDate
                ),
        });
        this.existpropertyDetailsData = false;
        this.arrangepropertyDetailsData = false;
      }
    }
  }

  existpropertyDetailsData = false;
  arrangepropertyDetailsData = false;
  selectexsitingPropertyChecked(e: any) {
    if (e === true) {
      this.existpropertyDetailsData = true;
      this.propertyFormGroup.patchValue({
        existingpropertyType: 'SFR',
        existingpropertyContracts: 'Yes',
        existingpropertyAddress1: 'SE-273 94 Tomelilla',
        existingpropertyCityName: 'Sweden',
        existingpropertyCountryName: 'Stockholm',
        existingpropertyZipCode: '273941',
        existingpropertyAddress2: 'Sverige',
        existingpropertyPrice: 504567.0,
        existingpropertyCurrency: 'EUR',
      });
    } else {
      this.existpropertyDetailsData = false;
    }
  }
  selectarrangePropertyChecked(e: any) {
    if (e === true) {
      this.arrangepropertyDetailsData = true;
    } else {
      this.arrangepropertyDetailsData = false;
    }
  }

  onSubmitPropertyData(num: number) {
    this.propertydetailSubmitted = true;

    if (this.propertyFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }

  // ======================= End Property Details =======================

  //==========================Loan Details =====================

  onChange(e: any) {
   // console.log(e.value);
    this.loanProductSelectBox = e.value;
  }

  setLoanDetailValue(data: any) {
    this.loanProductSelectBox =
      data.body.content.ProductDetails === undefined
        ? ''
        : data.body.content.ProductDetails.ProductID === undefined
        ? ''
        : data.body.content.ProductDetails.ProductID === ''
        ? ''
        : 'Shared';

    if (data.body == null || data.body.content.LoanInfo === undefined) {
      this.loanFormGroup.patchValue({
        loanPurpose: '',
        productType: '',
        productScope: '',
        checkMortgage: false,
        mortgageLoanAmount: 0,
        mortgageLoanCurrency: '',
        mortgageLoanSelfContAmount: 0,
        mortgageLoanSelfContCurrency: '',
        mortgageTimePeriod: 0,
        checkBridge: false,
        bridgeLoanAmount: 0,
        bridgeLoanAmountCurrency: '',
        bridgeLoanDate: '',
        bridgeTimePeriodinMonth: 0,
        downpaymentrequired: false,
        downpayLoanAmount: 0,
        downpayLoanAmountCurrency: '',
        downpaydate: '',
        downpaymentTimePeriodinYear: 0,
        creditCheck: false,
      });
      (this.mortgageChecked = false),
        (this.bridgeChecked = false),
        (this.downloadpaymentChecked = false);
    } else {
      if (data.body.content.pxCurrentStage != 'PRIM1') {
        this.options.disabled = true;
        this.options1.disabled = true;
        this.options2.disabled = true;

        this.loanFormGroup = this._formBuilder.group({
          loanPurpose: [
            {
              value:
                data.body.content.ProductDetails.ProductPurpose === undefined
                  ? ''
                  : data.body.content.ProductDetails.ProductPurpose === ''
                  ? ''
                  : data.body.content.ProductDetails.ProductPurpose,
              disabled: true,
            },
          ],
          productType: [
            {
              value:
                data.body.content.ProductDetails.ProductID === undefined
                  ? ''
                  : data.body.content.ProductDetails.ProductID === ''
                  ? ''
                  : data.body.content.ProductDetails.ProductID,
              disabled: true,
            },
          ],
          productScope: [
            {
              value:
                data.body.content.ProductDetails.pyNote === undefined
                  ? ''
                  : data.body.content.ProductDetails.pyNote === ''
                  ? ''
                  : data.body.content.ProductDetails.pyNote,
              disabled: true,
            },
          ],
          checkMortgage: [
            {
              value:
                data.body.content.LoanInfo.EndFinancingRequired === ''
                  ? false
                  : data.body.content.LoanInfo.EndFinancingRequired ===
                    undefined
                  ? false
                  : JSON.parse(data.body.content.LoanInfo.EndFinancingRequired),
              disabled: true,
            },
          ],
          mortgageLoanAmount: [
            {
              value:
                data.body.content.LoanInfo.TempLoanAmt === undefined
                  ? 0
                  : data.body.content.LoanInfo.TempLoanAmt === ''
                  ? ''
                  : parseFloat(data.body.content.LoanInfo.TempLoanAmt),
              disabled: true,
            },
          ],
          mortgageLoanCurrency: [
            {
              value:
                data.body.content.LoanInfo.LoanAmtCurrency === undefined
                  ? ''
                  : data.body.content.LoanInfo.LoanAmtCurrency === ''
                  ? ''
                  : data.body.content.LoanInfo.LoanAmtCurrency,
              disabled: true,
            },
          ],
          mortgageLoanSelfContAmount: [
            {
              value:
                data.body.content.LoanInfo.SelfContribution === undefined
                  ? 0
                  : data.body.content.LoanInfo.SelfContribution === ''
                  ? ''
                  : parseFloat(data.body.content.LoanInfo.SelfContribution),
              disabled: true,
            },
          ],
          mortgageLoanSelfContCurrency: [
            {
              value:
                data.body.content.LoanInfo.SelfContiCurrency === undefined
                  ? ''
                  : data.body.content.LoanInfo.SelfContiCurrency === ''
                  ? ''
                  : data.body.content.LoanInfo.SelfContiCurrency,
              disabled: true,
            },
          ],
          mortgageTimePeriod: parseInt(
            data.body.content.LoanInfo.EndFinancingLoanPeriod
          ),
          checkBridge: [
            {
              value:
                data.body.content.LoanInfo.isBridgeLoanRequired === undefined
                  ? false
                  : JSON.parse(data.body.content.LoanInfo.isBridgeLoanRequired),
              disabled: true,
            },
          ],
          bridgeLoanAmount: [
            {
              value: data.body.content.LoanInfo.TempBridgeLoanAmt,
              disabled: true,
            },
          ],
          bridgeLoanAmountCurrency: [
            {
              value: data.body.content.LoanInfo.BridgeLoanAmtCurrency,
              disabled: true,
            },
          ],
          bridgeLoanDate: [
            {
              value: this.formatDate(data.body.content.LoanInfo.BridgeLoanDate),
              disabled: true,
            },
          ],
          bridgeTimePeriodinMonth: parseInt(
            data.body.content.LoanInfo.BridgeLoanPeriod
          ),
          downpaymentrequired: [
            {
              value:
                data.body.content.LoanInfo.DownPaymentRequired === undefined
                  ? false
                  : JSON.parse(data.body.content.LoanInfo.DownPaymentRequired),
              disabled: true,
            },
          ],
          downpayLoanAmount: [
            {
              value: data.body.content.LoanInfo.TempDownPaymentAmt,
              disabled: true,
            },
          ],
          downpayLoanAmountCurrency: [
            {
              value: data.body.content.LoanInfo.DownPaymentAmtCurrency,
              disabled: true,
            },
          ],
          downpaydate: [
            {
              value: this.formatDate(
                data.body.content.LoanInfo.DownPaymentDate
              ),
              disabled: true,
            },
          ],
          downpaymentTimePeriodinYear: parseInt(
            data.body.content.LoanInfo.DownPaymentLoanPeriod
          ),
          creditCheck: [
            {
              value:
                data.body.content.CreditCheckPermit === undefined
                  ? false
                  : JSON.parse(data.body.content.CreditCheckPermit),
              disabled: true,
            },
          ],
        });
        (this.mortgageChecked =
          data.body.content.LoanInfo.EndFinancingRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.EndFinancingRequired)),
          (this.bridgeChecked =
            data.body.content.LoanInfo.isBridgeLoanRequired === undefined
              ? false
              : JSON.parse(data.body.content.LoanInfo.isBridgeLoanRequired)),
          (this.downloadpaymentChecked =
            data.body.content.LoanInfo.DownPaymentRequired === undefined
              ? false
              : JSON.parse(data.body.content.LoanInfo.DownPaymentRequired));
      }

      this.loanFormGroup.patchValue({
        loanPurpose:
          data.body.content.ProductDetails.ProductPurpose === undefined
            ? ''
            : data.body.content.ProductDetails.ProductPurpose === ''
            ? ''
            : data.body.content.ProductDetails.ProductPurpose,
        productType:
          data.body.content.ProductDetails.ProductID === undefined
            ? ''
            : data.body.content.ProductDetails.ProductID === ''
            ? ''
            : data.body.content.ProductDetails.ProductID,
        productScope:
          data.body.content.ProductDetails.pyNote === undefined
            ? ''
            : data.body.content.ProductDetails.pyNote === ''
            ? ''
            : data.body.content.ProductDetails.pyNote,
        checkMortgage:
          data.body.content.LoanInfo.EndFinancingRequired === ''
            ? false
            : data.body.content.LoanInfo.EndFinancingRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.EndFinancingRequired),
        mortgageLoanAmount:
          data.body.content.LoanInfo.TempLoanAmt === undefined
            ? 0
            : data.body.content.LoanInfo.TempLoanAmt === ''
            ? ''
            : parseFloat(data.body.content.LoanInfo.TempLoanAmt),
        mortgageLoanCurrency:
          data.body.content.LoanInfo.LoanAmtCurrency === undefined
            ? ''
            : data.body.content.LoanInfo.LoanAmtCurrency === ''
            ? ''
            : data.body.content.LoanInfo.LoanAmtCurrency,
        mortgageLoanSelfContAmount:
          data.body.content.LoanInfo.SelfContribution === undefined
            ? 0
            : data.body.content.LoanInfo.SelfContribution === ''
            ? ''
            : parseFloat(data.body.content.LoanInfo.SelfContribution),
        mortgageLoanSelfContCurrency:
          data.body.content.LoanInfo.SelfContiCurrency === undefined
            ? ''
            : data.body.content.LoanInfo.SelfContiCurrency === ''
            ? ''
            : data.body.content.LoanInfo.SelfContiCurrency,
        mortgageTimePeriod: parseInt(
          data.body.content.LoanInfo.EndFinancingLoanPeriod
        ),
        checkBridge:
          data.body.content.LoanInfo.isBridgeLoanRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.isBridgeLoanRequired),
        bridgeLoanAmount: data.body.content.LoanInfo.TempBridgeLoanAmt,
        bridgeLoanAmountCurrency:
          data.body.content.LoanInfo.BridgeLoanAmtCurrency,
        bridgeLoanDate: this.formatDate(
          data.body.content.LoanInfo.BridgeLoanDate
        ),
        bridgeTimePeriodinMonth: parseInt(
          data.body.content.LoanInfo.BridgeLoanPeriod
        ),
        downpaymentrequired:
          data.body.content.LoanInfo.DownPaymentRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.DownPaymentRequired),
        downpayLoanAmount: data.body.content.LoanInfo.TempDownPaymentAmt,
        downpayLoanAmountCurrency:
          data.body.content.LoanInfo.DownPaymentAmtCurrency,
        downpaydate: this.formatDate(
          data.body.content.LoanInfo.DownPaymentDate
        ),
        downpaymentTimePeriodinYear: parseInt(
          data.body.content.LoanInfo.DownPaymentLoanPeriod
        ),
        creditCheck:
          data.body.content.CreditCheckPermit === undefined
            ? false
            : JSON.parse(data.body.content.CreditCheckPermit),
      });
      (this.mortgageChecked =
        data.body.content.LoanInfo.EndFinancingRequired === undefined
          ? false
          : JSON.parse(data.body.content.LoanInfo.EndFinancingRequired)),
        (this.bridgeChecked =
          data.body.content.LoanInfo.isBridgeLoanRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.isBridgeLoanRequired)),
        (this.downloadpaymentChecked =
          data.body.content.LoanInfo.DownPaymentRequired === undefined
            ? false
            : JSON.parse(data.body.content.LoanInfo.DownPaymentRequired));
    }
  }

  mortgageChecked = false;
  bridgeChecked = false;
  downloadpaymentChecked = false;

  selectMortgage(e: any) {
    if (e === true) {
      this.mortgageChecked = true;
    } else {
      this.mortgageChecked = false;
    }
  }

  selectBridge(e: any) {
    if (e === true) {
      this.bridgeChecked = true;
    } else {
      this.bridgeChecked = false;
    }
  }

  selectDownPayment(e: any) {
    if (e === true) {
      this.downloadpaymentChecked = true;
    } else {
      this.downloadpaymentChecked = false;
    }
  }

  onSubmitLoanData(num: number) {
    this.loanSubmitted = true;
    if (this.loanFormGroup.invalid) {
      return;
    } else {
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }

  // ======================= End Loan Details =======================

  //===================Upload Document Details=================================
  myFiles: string[] = [];
  currentDateTime = [] as any;

  uploadDocumetRowItems(): FormArray {
    return this.uploadFormGroup.get('uploadDocumetRowItems') as FormArray;
  }

  newUploadDocument(): FormGroup {
    return this._formBuilder.group({
      documents: ['', [Validators.required]],
      uploadFile: ['', [Validators.required]],
      uploadedBy: '',
      dateTime: '',
    });
  }

  onFileChange(event: any, j: number) {
    this.currentDateTime[j] = this.datepipe.transform(
      new Date(),
      'MM/dd/yyyy h:mm:ss'
    );

    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
  }

  addFileUploadField() {
    this.uploadDocumetRowItems().push(this.newUploadDocument());
  }

  removeUploadFile(i: number) {
    this.uploadDocumetRowItems().removeAt(i);
  }
  //==================== document upload data ===============================
  onSubmitDocumentUploadData(num: number) {
    this.uploadDocumentSubmitted = true;
    // this.uploadFormGroup.invalid ||
    //   this.uploadFormGroup.get('uploadDocumetRowItems')?.value.length === 0
    let dataUpload = 'file';
    if (dataUpload.length < 0) {
      this.uploadDocumentSubmitted = true;
      return;
    } else {
      this.uploadDocumentSubmitted = false;
      if (this.nxtBtnShowafterPM === true) {
        this.step = num;
        setTimeout(() => (this.stepper.selectedIndex = num - 1));
      } else {
        let returnReqData = this.commonRequestData(
          this.firstFormGroup.value,
          this.secondFormGroup.value,
          this.productForm.value,
          this.propertyFormGroup.value,
          this.loanFormGroup.value,
          this.uploadFormGroup.value
        );

        this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
          (updata) => {
            if (updata.status === 204) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', updata.headers.get('etag'));
              this.getCaseApiCall();
              let str = this.route.snapshot.params['caseId'];
              let caseName = str.substring(str.lastIndexOf(' ') + 1);
              // this.notificationService.showSuccess(
              //   'Case updated successfully',
              //   `${caseName}`
              // );
              this.step = num;
              setTimeout(() => (this.stepper.selectedIndex = num - 1));
            }
          },
          (error) => {
            let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
            this.notificationService.showError(
              `${errorMessage}`,
              'Server error occurs'
            );
          }
        );

        // this.caseService.getCaseById(this.caseId).subscribe(
        //   (resp) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       localStorage.removeItem('etag');
        //       localStorage.setItem('etag', resp.headers.get('etag'));

        //     }
        //   },
        //   (error) => {
        //     console.log('get_api_call_before_update_error', error);
        //     this.notificationService.showError(
        //       `${error.message}`,
        //       'Server error occurs'
        //     );
        //   }
        // );
      }
    }
  }
  //============================= review data submit ===========================
  onSubmitReviewdData() {
    let returnReqData = this.commonRequestData(
      this.firstFormGroup.value,
      this.secondFormGroup.value,
      this.productForm.value,
      this.propertyFormGroup.value,
      this.loanFormGroup.value,
      this.uploadFormGroup.value
    );

    this.caseService.updateCaseById(this.caseId, returnReqData).subscribe(
      (updata) => {
        //console.log(updata);
        if (updata.status === 204) {
          let str = this.route.snapshot.params['caseId'];
          let caseName = str.substring(str.lastIndexOf(' ') + 1);

          this.caseService.createCalculateLaonById(this.caseId).subscribe(
            (calresp) => {
             // console.log(calresp);
              if (calresp.status === 204) {
                localStorage.removeItem('etag');
                localStorage.setItem('etag', calresp.headers.get('etag'));
                this.caseService
                  .updateChangeStateById(this.caseId, this.changeStateData())
                  .subscribe(
                    (respChange) => {
                     // console.log(respChange);
                      if (respChange.status === 204) {
                        this.notificationService.showSuccess(
                          'Case updated successfully',
                          `${caseName}`
                        );
                        this.router.navigateByUrl(
                          `/consumer/confirm/${caseName}`
                        );
                      }
                    },
                    (error) => {
                      this.notificationService.showError(
                        `${error.message}`,
                        'Server error occurs'
                      );
                    }
                  );
              }
            },
            (error) => {
              this.notificationService.showError(
                `${error.message}`,
                'Server error occurs'
              );
            }
          );
        }
      },
      (error) => {
        let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
        this.notificationService.showError(
          `${errorMessage}`,
          'Server error occurs'
        );
      }
    );

    // this.caseService.getCaseById(this.caseId).subscribe(
    //   (resp) => {
    //     console.log(resp);
    //     if (resp.status === 200) {
    //       localStorage.removeItem('etag');
    //       localStorage.setItem('etag', resp.headers.get('etag'));

    //     }
    //   },
    //   (error) => {
    //     console.log('get_api_call_before_update_error', error);
    //     this.notificationService.showError(
    //       `${error.errors[0].message}`,
    //       'Server error occurs'
    //     );
    //   }
    // );
  }

  //===================== KYC Verify =======================================

  kycVerify(e: any) {
    if (e === 'Green') {
      this.kycVerifyData = 'Green';

      let returnReqData = this.commonRequestData(
        this.firstFormGroup.value,
        this.secondFormGroup.value,
        this.productForm.value,
        this.propertyFormGroup.value,
        this.loanFormGroup.value,
        this.uploadFormGroup.value
      );

      this.caseService
        .getCaseById(this.route.snapshot.params['caseId'])
        .subscribe(
          (resp) => {
            if (resp.status === 200) {
              localStorage.removeItem('etag');
              localStorage.setItem('etag', resp.headers.get('etag'));
              this.caseService
                .updateCaseById(this.caseId, returnReqData)
                .subscribe(
                  (updata) => {
                    if (updata.status === 204) {
                      this.getCaseApiCall();
                      this.greenRedFlag = true;
                      let str = this.route.snapshot.params['caseId'];
                      let caseName = str.substring(str.lastIndexOf(' ') + 1);
                      this.notificationService.showSuccess(
                        'KYC Verified successfully',
                        `${caseName}`
                      );
                      //this.nxtBtnShow = true;
                      // this.router.navigateByUrl('/consumer/dashboard');
                    }
                  },
                  (error) => {
                    let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
                    this.notificationService.showError(
                      `${errorMessage}`,
                      'Server error occurs'
                    );
                  }
                );
            }
          },
          (error) => {
            this.notificationService.showError(
              `${error.message}`,
              'Server error occurs'
            );
          }
        );
    }
  }

  //===================== End KYC Verify ======================================

  // ========================== Request Data format ===============================

  commonRequestData(
    borrowData: any,
    emplyData: any,
    finanData: any,
    propertyData: any,
    loanData: any,
    document: any
  ) {
    //console.log(borrowData, emplyData, propertyData, loanData);

    let coappCheckFirstName =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantfirstName
        : '';
    let coappCheckLastName =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantlastName
        : '';
    let coappCheckContactNumber =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantcontactNumber
        : '';
    let coappCheckEmailAddress =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantemailAddress
        : '';
    let coappCheckAddressLine1 =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantaddressLine1
        : '';
    let coappCheckAddressLine2 =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantaddressLine2
        : '';
    let coappCheckCityName =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantcityName
        : '';
    let coappCheckCountryName =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantcountryName
        : '';
    let coappCheckZipCode =
      borrowData.isCoApplicantCked === true
        ? borrowData.coapplicantzipCode
        : '';

    //======================== employee page co-applicant data===================

    let coappCheckEmployType =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_employeeType
        : '';
    let coappCheckEmployerName =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_employerName
        : '';
    let coappCheckEmployPosition =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_position
        : '';
    let coappCheckEmployStartDate =
      borrowData.isCoApplicantCked === true
        ? this.formatDateGMT(emplyData.coapplicant_startDate)
        : '';
    let coappCheckEmployCityName =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_cityName
        : '';
    let coappCheckEmployCountryName =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_countryName
        : '';
    let coappCheckEmployPincode =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_pinCode
        : '';
    let coappCheckEmployAddresLine1 =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_addressLine1
        : '';
    let coappCheckEmployContactNumber =
      borrowData.isCoApplicantCked === true
        ? emplyData.coapplicant_contactNumber
        : '';

    // ==================calculation total liabilities, Recurring, Source Income=======================
    function sumOfCustomerLiabilitAndCoapplicantLiabilit(datat: any) {
      let valueInBeginning = 0;
      let sumOfLiabilities = datat.reduce(
        (accumVariable: any, curValue: { LiabilityAmount: string }) =>
          accumVariable + parseFloat(curValue.LiabilityAmount),
        valueInBeginning
      );
      return sumOfLiabilities;
    }
    function sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(datat: any) {
      let valueInBeginning = 0;
      let sumOfLiabilities = datat.reduce(
        (accumVariable: any, curValue: { IncomeValue: string }) =>
          accumVariable + parseFloat(curValue.IncomeValue),
        valueInBeginning
      );
      return sumOfLiabilities;
    }
    function sumOfCustomerRecurringCostAndCoapplicantRecurringCost(datat: any) {
      let valueInBeginning = 0;
      let sumOfLiabilities = datat.reduce(
        (accumVariable: any, curValue: { ExpenseAmount: string }) =>
          accumVariable + parseFloat(curValue.ExpenseAmount),
        valueInBeginning
      );
      return sumOfLiabilities;
    }

    let formatData = {
      content: {
        CoApplicantCheck: borrowData.isCoApplicantCked, // borrower page co-applicant check box
        CreditCheckPermit: loanData.creditCheck, // loan page  creadit check box
        Currency: 'INR',
        CustomerStatus: 'NewCustomer',
        ExistingCustomerRadio: 'Yes',
        IsExistingCustomer: 'true',
        IsManualHandlingDone: 'false',
        KYCColour: this.kycVerifyData,
        LoanToValue: '0.00',
        SearchCustomerId: '890102-3287',
        SelectedCustomerCifNbr: '8901023289',
        SFFlag: 'true',
        TempDebtToIncome: '0.0387331487591052',
        TotalExpense:
          sumOfCustomerRecurringCostAndCoapplicantRecurringCost(
            finanData.recurries
          ) +
          sumOfCustomerRecurringCostAndCoapplicantRecurringCost(
            finanData.coapplicant_recurries
          ) +
          sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.liabilities) +
          sumOfCustomerLiabilitAndCoapplicantLiabilit(
            finanData.coapplicant_liabilities
          ), //TotalRunningCost + TotalLiabilities
        TotalBaseIncome:
          finanData.totalIncome + borrowData.isCoApplicantCked === true
            ? finanData.coapplicant_totalIncome
            : 0, // sum of customer Income.BaseEmplIncome + sum of co-applicant Income.BaseEmplIncome
        TotalLiabilities:
          sumOfCustomerLiabilitAndCoapplicantLiabilit(finanData.liabilities) +
          sumOfCustomerLiabilitAndCoapplicantLiabilit(
            finanData.coapplicant_liabilities
          ), //sum of customer LiabilitiesList.liabilitiesAmount + sum of co-applicant LiabilitiesList.liabilitiesAmount
        TotalOtherSourceOfIncome:
          sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
            finanData.monthlies
          ) +
          sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
            finanData.coapplicant_monthlies
          ), //sum of customer IncomeList.IncomeValue + sum of co-applicant IncomeList.IncomeValue
        TotalRunningCost:
          sumOfCustomerRecurringCostAndCoapplicantRecurringCost(
            finanData.recurries
          ) +
          sumOfCustomerRecurringCostAndCoapplicantRecurringCost(
            finanData.coapplicant_recurries
          ), //sum of customer ExpensesList.ExpenseAmount + sum of co-applicant ExpensesList.ExpenseAmount
        TotalIncome:
          finanData.totalIncome +
            sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
              finanData.monthlies
            ) +
            sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
              finanData.coapplicant_monthlies
            ) +
            borrowData.isCoApplicantCked ===
          true
            ? finanData.coapplicant_totalIncome
            : 0, //TotalBaseIncome + TotalOtherSourceOfIncome
        CreditDetails: {},
        ExistingProperty: {
          CollateralPrice: '378425.25',
          CurrencyCodesList:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.saleCurrency
              : '',
          TransferDate:
            propertyData.exsitingPropertyChecked === true
              ? this.formatDateGMT(propertyData.transferDate)
              : '',
          IsSold:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.arrangePropertyChecked
              : false,
          PropertyPrice:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.existingpropertyPrice
              : '',
          PropertyType:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.existingpropertyType
              : '',
          SalesAmount:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.saleAmount
              : 0,
          SignedContractForSale:
            propertyData.exsitingPropertyChecked === true
              ? propertyData.existingpropertyContracts
              : '',
          Address: {
            AddressLine1:
              propertyData.exsitingPropertyChecked === true
                ? propertyData.existingpropertyAddress1
                : '',
            AddressLine2:
              propertyData.exsitingPropertyChecked === true
                ? propertyData.existingpropertyAddress2
                : '',
            City:
              propertyData.exsitingPropertyChecked === true
                ? propertyData.existingpropertyCityName
                : '',
            Country:
              propertyData.exsitingPropertyChecked === true
                ? propertyData.existingpropertyCountryName
                : '',
            ZipCode:
              propertyData.exsitingPropertyChecked === true
                ? propertyData.existingpropertyZipCode
                : '',
          },
        },
        LoanInfo: {
          BridgeLoanAmtCurrency:
            loanData.checkBridge === true
              ? loanData.bridgeLoanAmountCurrency
              : '', // bridge loan currency
          BridgeLoanDate:
            loanData.checkBridge === true
              ? this.formatDateGMT(loanData.bridgeLoanDate)
              : '', // bridge loan date
          BridgeLoanPeriod:
            loanData.checkBridge === true
              ? loanData.bridgeTimePeriodinMonth
              : 0, // bridge Loan time period in months
          DownPaymentAmtCurrency:
            loanData.downpaymentrequired === true
              ? loanData.downpayLoanAmountCurrency
              : '', // down payment currency
          DownPaymentDate:
            loanData.downpaymentrequired === true
              ? this.formatDateGMT(loanData.downpaydate)
              : '', // down payment date
          DownPaymentLoanPeriod:
            loanData.downpaymentrequired === true
              ? loanData.downpaymentTimePeriodinYear
              : 0, // down payment time period in yrs
          DownPaymentRequired: loanData.downpaymentrequired, // down payment check box required
          //EMIForBridgeLoan: '0.4532533232640007',
          //EMIForMortgageLoan: '0.0000',
          EndFinancingLoanPeriod:
            loanData.checkMortgage === true ? loanData.mortgageTimePeriod : 0, // Mortgage Loan time period in yrs
          //EndFinancingLoanPeriodInitial: '5',
          EndFinancingRequired: loanData.checkMortgage, //Mortgage Loan required check box
          //InterestRateForBridgeLoan: '4',
          //InterestRateForDownPaymentLoan: '5.0',
          //InterestRateForMortgageLoan: '5',
          isBridgeLoanRequired: loanData.checkBridge, // bridge loan required check box
          LoanAmtCurrency:
            loanData.checkMortgage === true
              ? loanData.mortgageLoanCurrency
              : '', // mortage loan currency
          SelfContiCurrency:
            loanData.checkMortgage === true
              ? loanData.mortgageLoanSelfContCurrency
              : '',
          SelfContribution:
            loanData.checkMortgage === true
              ? loanData.mortgageLoanSelfContAmount
              : 0, // Mortgage self contribution amount
          TempBridgeLoanAmt:
            loanData.checkBridge === true ? loanData.bridgeLoanAmount : 0, // bridge Loan amount
          TempDownPaymentAmt:
            loanData.downpaymentrequired === true
              ? loanData.downpayLoanAmount
              : 0, // down payment amount
          TempLoanAmt:
            loanData.checkMortgage === true ? loanData.mortgageTimePeriod : 0, // Mortgage Loan amount
        },
        ProductDetails: {
          ProductPurpose: loanData.loanPurpose,
          ProductID: loanData.productType,
          pyNote: loanData.productScope,
        },
        PropertyDetails: {
          CollateralPrice: '2.25',
          CurrencyCodesList: propertyData.propertyCurrency,
          HasExistingProperty: propertyData.exsitingPropertyChecked,
          PropertyPrice: propertyData.propertyPrice,
          PropertyType: propertyData.PropertyType,
          PropertyStatus: propertyData.propertyStatus,
          Address: {
            AddressLine1: propertyData.addressLine1,
            AddressLine2: propertyData.addressLine2,
            City: propertyData.cityName,
            Country: propertyData.countryName,
            ZipCode: propertyData.zipCode,
          },
        },
        pyWorkParty: {
          CoApplicant: {
            CifNbr: '890102-3287',
            DateOfBirth: '19800115',
            FirstName: coappCheckFirstName,
            FullName: coappCheckFirstName + ' ' + coappCheckLastName,
            LastName: coappCheckLastName,
            pxPartyRole: 'CoApplicant',
            pxSubscript: 'CoApplicant',
            pyEmail1: coappCheckEmailAddress,
            pyLabel: ' ',
            pyMobilePhone: coappCheckContactNumber,
            pyWorkPartyUri: '890102-3287',
            pzIndexOwnerKey: this.caseId,
            SumLiabilities: sumOfCustomerLiabilitAndCoapplicantLiabilit(
              finanData.coapplicant_liabilities
            ), //sum of LiabilitiesList.liabilitiesAmount
            TotalIncome: '0.00',
            TotalOtherSourceIncome:
              sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
                finanData.coapplicant_monthlies
              ), //sum of IncomeList.IncomeValue
            pxObjClass: 'CG-Data-Party-Ind',
            Address: {
              Home: {
                AddressLine1: coappCheckAddressLine1,
                AddressLine2: coappCheckAddressLine2,
                City: coappCheckCityName,
                Country: coappCheckCountryName,
                ZipCode: coappCheckZipCode,
              },
              Work: {
                City: 'Melbourne',
                Country: 'Australia',
                StateCode: 'AL',
                ZipCode: '76142',
              },
            },
            Assets:
              borrowData.isCoApplicantCked === true
                ? finanData.coapplicant_quantities
                : [],
            EmploymentHistory: [
              {
                EmployerName: coappCheckEmployerName,
                EmployerWorkPhone: coappCheckEmployContactNumber,
                Title: coappCheckEmployPosition,
                TypeOfEmployment: coappCheckEmployType,
                WorkStartDate: coappCheckEmployStartDate, //'20211109T050000.000 GMT'
                Address: {
                  AddressLine1: coappCheckEmployAddresLine1,
                  City: coappCheckEmployCityName,
                  Country: coappCheckEmployCountryName,
                  ZipCode: coappCheckEmployPincode,
                },
              },
            ],
            ExpensesList:
              borrowData.isCoApplicantCked === true
                ? finanData.coapplicant_recurries
                : [],
            Income: {
              BaseEmplIncome:
                borrowData.isCoApplicantCked === true
                  ? finanData.coapplicant_totalIncome
                  : 0,
            },
            IncomeList:
              borrowData.isCoApplicantCked === true
                ? finanData.coapplicant_monthlies
                : [],
            LiabilitiesList:
              borrowData.isCoApplicantCked === true
                ? finanData.coapplicant_liabilities
                : [],
          },
          Customer: {
            CifNbr: '8901023289',
            CustomerID: '8901023289',
            DisplayCustomerDetails: 'true',
            FirstName: borrowData.firstName,
            KYCColour: this.kycVerifyData,
            LastName: borrowData.lastName,
            pyEmail1: borrowData.emailAddress,
            pyMobilePhone: borrowData.contactNumber,
            pyWorkPartyUri: 'gkuzu@yahoo.com',
            ShowPartyType: 'All party types',
            ShowStatus: 'ActiveandInActive',
            SumLiabilities: sumOfCustomerLiabilitAndCoapplicantLiabilit(
              finanData.liabilities
            ), // sum of LiabilitiesList.liabilitiesAmount
            TotalIncome: '0.00',
            TotalOtherSourceIncome:
              sumOfCustomerSourceIncomeAndCoapplicantSourceIncome(
                finanData.monthlies
              ), //sum of IncomeList.IncomeValue
            pxObjClass: 'CG-Data-Party-Ind',
            Address: {
              Home: {
                AddressLine1: borrowData.addressLine1,
                AddressLine2: borrowData.addressLine2,
                City: borrowData.cityName,
                Country: borrowData.countryName,
                ZipCode: borrowData.zipCode,
              },
            },
            Assets: finanData.quantities,
            EmploymentHistory: [
              {
                EmployerName: emplyData.employerName,
                EmployerWorkPhone: emplyData.contactNumber,
                Title: emplyData.position,
                TypeOfEmployment: emplyData.employeeType,
                WorkStartDate: this.formatDateGMT(emplyData.startDate),
                Address: {
                  AddressLine1: emplyData.addressLine1,
                  City: emplyData.cityName,
                  Country: emplyData.countryName,
                  ZipCode: emplyData.pinCode,
                },
              },
            ],
            ExpensesList: finanData.recurries,
            Income: {
              BaseEmplIncome: finanData.totalIncome,
            },
            IncomeList: finanData.monthlies,
            LiabilitiesList: finanData.liabilities,
          },
        },
      },
    };

    return formatData;
  }

  //======================= change state data format==============================
  changeStateData() {
    let changedata = {
      content: {
        pyAuditNote: 'Proceed to next stage after Colect Application Data',
        pyChangeStageOption: 'goto',
        pyGotoStage: 'Risk Analysis',
      },
    };
    return changedata;
  }
}

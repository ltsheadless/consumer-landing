import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import {MatMenuTrigger} from '@angular/material/menu';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../authservice/auth.service';
import { NotificationService } from '../notifications/notification.service';
import { CaseService } from './services/case.service';

@Component({
  selector: 'app-consumercase',
  templateUrl: './consumercase.component.html',
  styleUrls: ['./consumercase.component.css'],
})
export class ConsumercaseComponent implements OnInit {
  showFiller = false;
  userData: any;

  @ViewChild('menuTrigger')
  menuTrigger!: MatMenuTrigger;

  constructor(
    public authService: AuthService,
    private spinner: NgxSpinnerService,
    private caseService: CaseService,
    private notificationService: NotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 3000);


    let data: any =localStorage.getItem('users');
       this.userData=JSON.parse(data)

    //=============================get all cases============================
    this.getAllCases();
  }

  getAllCases() {
    this.caseService.getAllCases().subscribe(
      (data) => {
       // console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //==========================new loan create ==============================
  refreshGridInAnotherComponent() {
   
this.router.routeReuseStrategy.shouldReuseRoute = function () {
  return false
}
this.router.onSameUrlNavigation = 'reload';

this.caseService.createCase(this.createcaseData()).subscribe(
      (data)=>{
        console.log(data)
       if (data.status===201) {
        let caseTd = data.body.ID
        this.router.navigateByUrl(`/consumer/case-detail/${caseTd}`)
       }
      },
      (error) => {
        console.log('create_data_error', error);
        let errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.errors[0].message}`;
        this.notificationService.showError(
          `${errorMessage}`,
          'Server error occurs'
        );
      }
    ) 


 }


  createcaseData(){
    let caseData = {
      
        "caseTypeID": "CG-FW-Consumer-Work-Mortgage-LoanOrigination",
        "processID": "pyStartCase",
        "parentCaseID": "",
        "content": {
          "KYCColour":"Gray",
          "CreationChannel": "API",
          "pyWorkParty": {
            "CoApplicant": {
              "pxObjClass": "CG-Data-Party-Ind"
            },
            "Customer": {
              "CifNbr":"8901023289",
              "CustomerID":"8901023289",
              "KYCColour":"Gray",
              "pxObjClass": "CG-Data-Party-Ind"
            }
          }
        }
      
     }
     return caseData;
   }

 


  logout() {
    this.authService.logout();
  }
}

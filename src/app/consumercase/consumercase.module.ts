import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { ConsumercaseRoutingModule } from './consumercase-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConsumercaseComponent } from './consumercase.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AllMaterialModule } from '../all-material/all-material.module';
import { BoostrapmoduleModule } from '../boostrapmodule/boostrapmodule.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CasedetailComponent } from './casedetail/casedetail.component';
import { CaseService } from './services/case.service';
import { MyHttpInterceptor } from '../http.interceptor';
import { MatCurrencyFormatModule } from 'mat-currency-format';
import { ToastrModule } from 'ngx-toastr';
import { CurrencyInputDirective } from './currency-input.directive';

@NgModule({
  declarations: [
    DashboardComponent,
    ConsumercaseComponent,
    CasedetailComponent,
    CurrencyInputDirective
  ],
  imports: [
    CommonModule,
    ConsumercaseRoutingModule,
    AllMaterialModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    BoostrapmoduleModule,
    NgxSpinnerModule,
    MatCurrencyFormatModule,
    ToastrModule,
    
  ],
  providers: [CaseService, CurrencyPipe, {
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true,
  },],
})
export class ConsumercaseModule {}

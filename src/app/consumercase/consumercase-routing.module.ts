import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasedetailComponent } from './casedetail/casedetail.component';
import { ConfirmpageComponent } from './confirmpage/confirmpage.component';
import { ConsumercaseComponent } from './consumercase.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', component: ConsumercaseComponent ,
  children: [ 
    {path: 'dashboard', component: DashboardComponent },
    {path: 'case-detail/:caseId', component: CasedetailComponent },
    {path: 'confirm/:caseId', component: ConfirmpageComponent },
    
  ]
 
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsumercaseRoutingModule { }

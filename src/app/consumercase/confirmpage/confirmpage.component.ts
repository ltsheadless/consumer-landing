import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirmpage',
  templateUrl: './confirmpage.component.html',
  styleUrls: ['./confirmpage.component.css']
})
export class ConfirmpageComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
 caseName: any;
  ngOnInit(): void {
   this.caseName = this.route.snapshot.params['caseId'];
  }

}
